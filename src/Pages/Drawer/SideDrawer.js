import React, { useState } from 'react';
import { Drawer, IconButton, List, ListItem, ListItemIcon, ListItemText } from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import Typography from "@mui/material/Typography";
import "./SideDrawer.css";
import DashboardRoundedIcon from '@mui/icons-material/DashboardRounded';
import InventoryRoundedIcon from '@mui/icons-material/InventoryRounded';
import Inventory2RoundedIcon from '@mui/icons-material/Inventory2Rounded';
import StoreRoundedIcon from '@mui/icons-material/StoreRounded';
import SupervisorAccountRoundedIcon from '@mui/icons-material/SupervisorAccountRounded';
import LocalShippingRoundedIcon from '@mui/icons-material/LocalShippingRounded';
import SupportAgentRoundedIcon from '@mui/icons-material/SupportAgentRounded';
import LiveHelpRoundedIcon from '@mui/icons-material/LiveHelpRounded';
import { useNavigate } from "react-router-dom";
import Link from "@mui/material/Link";

const menuItem = [
    {
        text: 'Dashboard',
        icon: <DashboardRoundedIcon />,
        to: '/dashboard',
        href: "/dashboard"
    },
    {
        text: 'Inventory',
        icon: <Inventory2RoundedIcon />,
        to: '/manage-inventory',
        href: "/manage-inventory"
    },
    {
        text: 'Suppliers',
        icon: <StoreRoundedIcon />,
        to: '/suppliers',
        href: "/suppliers"
    },
    {
        text: 'Clients',
        icon: <SupervisorAccountRoundedIcon />,
        to: '/manage-clients',
        href:"/manage-clients"
    },
    {
        text: 'Deliveries',
        icon: <LocalShippingRoundedIcon />,
        to: '/deliveries',
        href:"/deliveries"
    },
    {
        text: 'Invoice',
        icon: <InventoryRoundedIcon />,
        to: '/invoice',
        href:'/invoice'
    },
    {
        text: 'Support',
        icon: <SupportAgentRoundedIcon />,
        to: '/support',
        href:'/support'
    },
    {
        text: 'FAQs',
        icon: <LiveHelpRoundedIcon />,
        to: '/faqs',
        href:'/faqs'
    }
];

const SideDrawer = () => {
    const navigate = useNavigate();
    const [isDrawerOpen, setIsDrawerOpen] = useState(false);

    return (
        /*Side SideDrawer Begin*/
        <>
            <IconButton size = 'large' edge = 'start' aria-label = 'logo' onClick = {() => {
                setIsDrawerOpen(true);
            }}>
                <MenuIcon />
            </IconButton>
            <Drawer
                anchor = "left"
                open = {isDrawerOpen}
                onClose = {() => {
                    setIsDrawerOpen(false);
                }}
                classes = {{ paper: 'drawerPaper' }}
                PaperProps = {{
                    sx: {
                        backgroundColor: "#212F3C",
                        color: "whitesmoke",
                    }
                }}
            >
                <Typography onClick = {() => navigate('/')} variant = "h5" sx = {{ fontWeight: 'bold' }}
                            className = "title-center">
                    Ship<span className = "split-color">ido</span>
                    <ChevronLeftIcon onClick={()=> setIsDrawerOpen(false)} sx={{marginLeft:'2rem'}}/>
                </Typography>

                {/*list / links*/}
                <List>
                    {
                        menuItem?.map((item, index) => (

                            <Link
                                key = {item.text}
                                className = "icon-color"
                                variant = "button"
                                color = "text.primary"
                                href = {item.href}
                                // to = {item.path}
                                sx = {{ textDecoration: 'none', textTransform: "none" }}
                            >
                                <ListItem
                                    button
                                    key = {index}
                                >
                                    <ListItemIcon className = "icon-color">{item.icon}</ListItemIcon>
                                    <ListItemText primary = {item.text} />
                                </ListItem>
                            </Link>
                        ))
                    }
                </List>
            </Drawer>
        </>
    );
};

export default SideDrawer;
