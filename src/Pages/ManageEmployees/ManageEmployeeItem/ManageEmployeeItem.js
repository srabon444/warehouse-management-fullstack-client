import React from 'react';
import "./ManageEmployeeItem.css";
import { useNavigate } from "react-router-dom";
import { Chip } from "@mui/material";
import EditRoundedIcon from "@mui/icons-material/EditRounded";
import DeleteForeverRoundedIcon from "@mui/icons-material/DeleteForeverRounded";
import StyledTableCell from "../../../StyledComponents/StyledTableCell";

const ManageEmployeeItem = ({ index, employee, deleteEmployee }) => {
    const {
        _id,
        first_name,
        last_name,
        position,
        email,
        gender,
        image,
        mobile_number,
        emergency_contact,
        joining_date,
        social_security
    } = employee;
    // console.log({ employee });

    // Full Name
    // const fullName = `${first_name} + &nbsp; + ${last_name}`;

    const navigate = useNavigate();
    return (
        <>
            <StyledTableCell sx = {{ fontWeight: 'bold' }} align = "left">
                {index + 1}.</StyledTableCell>
            <StyledTableCell  align = "left" sx = {{ fontWeight: 'bold' }} scope = "row">{first_name}  &nbsp;  {last_name}</StyledTableCell>

            <StyledTableCell width = "2rem" align = "left">
                <img width = "100%" src = {image} alt = "" />
            </StyledTableCell>
            <StyledTableCell align = "left">
                <Chip
                    sx = {{ fontWeight: 'bold', color: 'white', backgroundColor: position === 'CEO' ? '#3D8361' : '#BA94D1' }}
                    label = {position}
                    variant = "contained"
                />
            </StyledTableCell>
            <StyledTableCell align = "left">{email}</StyledTableCell>
            <StyledTableCell align = "left">
                <Chip
                    sx = {{ color: 'white', backgroundColor: gender === 'Female' ? '#f39abd' : '#798777' }}
                    label = {gender}
                    variant = "contained"
                />
            </StyledTableCell>
            {/*<StyledTableCell align = "right">${address}</StyledTableCell>*/}
            <StyledTableCell align = "left">{mobile_number}</StyledTableCell>
            {/*<StyledTableCell align = "right">{marital_status}</StyledTableCell>*/}
            <StyledTableCell align = "left">{emergency_contact}</StyledTableCell>
            {/*<StyledTableCell align = "right">{birth_date}</StyledTableCell>*/}
            <StyledTableCell align = "left">{joining_date}</StyledTableCell>
            <StyledTableCell align = "left">{social_security}</StyledTableCell>

            <StyledTableCell align = "right">
                <EditRoundedIcon
                    sx = {{ color: '#1976D2', cursor: 'pointer' }}
                    onClick = {() => navigate(`/employee/${_id}`)}
                />{"  "}

                <DeleteForeverRoundedIcon
                    sx = {{ color: 'red', cursor: 'pointer' }}
                    onClick = {() => deleteEmployee(_id)}
                />
            </StyledTableCell>
        </>
    );
};

export default ManageEmployeeItem;
