import React from 'react';
import "./AddEmployees.css";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import Header from "../../Shared/Header/Header";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import { Button, Container, Form } from "react-bootstrap";
import "./AddEmployees.css";

const AddEmployees = () => {
    const { register, handleSubmit, reset } = useForm();

    const baseUrl = process.env.REACT_APP_BASE_URL;

    const onSubmit = data => {
        const url = `${baseUrl}/employee`;
        fetch(url, {
            method: "POST",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result.insertedId) {
                    toast.success('New Employee has Been Added');
                    reset();
                }
            });
    };


    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container w-50 mx-auto">
                <PageTitle title = {"Add Employee"}></PageTitle>

                <Container>
                    <Form className = "add-employee-border" onSubmit = {handleSubmit(onSubmit)}>
                        <h4 className = " title-color text-center mb-4"
                            style = {{ fontWeight: "bold", marginTop: "1rem" }}>Add New Employee</h4>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text"
                                          placeholder = "First Name" {...register("first_name", {
                                required: true, maxLength: 20
                            })} />
                            <Form.Control type = "text" style = {{ marginLeft: "1rem" }}
                                          placeholder = "Last Name" {...register("last_name", {
                                required: true, maxLength: 20
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "email"
                                          placeholder = "Email" {...register("email", {
                                required: true, pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                            })} />
                        </Form.Group>


                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicBrand">
                            <Form.Control type = "text"
                                          placeholder = "Position" {...register("position", {
                                required: true
                            })} />


                            <Form.Select style = {{ marginLeft: "1rem" }} id = "categoryList" {...register("gender", {
                                required: true
                            })}>
                                <option disabled selected label="Gender"/>
                                {
                                    ["Male", "Female"]?.map((gender, index) => <option
                                        key = {index}
                                    >{gender}
                                    </option>)
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicPrice">
                            <Form.Control type = "number"
                                          placeholder = "Phone Number" {...register("mobile_number", {
                                required: true,
                                minLength:8,
                                maxLength: 12
                            })} />

                            <Form.Control style = {{ marginLeft: "1rem" }} type = "number"
                                          placeholder = "Emergency Number" {...register("emergency_contact", {
                                required: true,
                                minLength:8,
                                maxLength: 12
                            })} />

                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicBrand">
                            <Form.Select id = "categoryList" {...register("marital_status", {
                                required: true
                            })}>
                                <option disabled selected label="Marital Status"/>
                                {
                                    ["Single", "Married"]?.map((marital, index) => <option
                                        key = {index}
                                    >{marital}
                                    </option>)
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "description">
                            <Form.Control placeholder = "Address" as = "textarea"
                                          rows = {3} {...register("address", {
                                required: true
                            })} />
                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicStock">
                            <Form.Control type = "number"
                                          placeholder = "Social Security" {...register("social_security", {
                                required: true,
                                minLength: 8,
                                maxLength: 10
                            })} />
                        </Form.Group>

                        <Form.Group className = " mb-3 col-lg forms-control" controlId = "formBasicStock">
                            <Form.Label>Birth Date</Form.Label>
                            <Form.Control className = " mb-3" type = "date"
                                          placeholder = "Birth Date" {...register("birth_date", {
                                required: true,
                                pattern: /(^0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[0-2])-(\d{4}$)/
                                // valueAsDate: true,
                            })} />
                            <Form.Label>Joining Date</Form.Label>
                            <Form.Control type = "date"
                                          placeholder = "Joining Date" {...register("joining_date", {
                                required: true,
                                pattern: /(^0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[0-2])-(\d{4}$)/
                                // valueAsDate: true,
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text"
                                          placeholder = "Image URL" {...register("image", {
                                required: true
                            })} />
                        </Form.Group>

                        <Button className = "add-btn" style = {{ width: "60%" }}
                                variant = "primary mx-auto d-block mb-5" type = "submit">
                            Add Employee
                        </Button>
                    </Form>
                </Container>
            </div>
        </>
    );
};

export default AddEmployees;
