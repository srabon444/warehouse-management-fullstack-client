import React, { useEffect, useState } from 'react';
import "./ManageEmployees.css";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import {
    Box,
    Button,
    Grid,
    Table,
    TableBody,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import Header from "../../Shared/Header/Header";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Loading from "../../Shared/Loading/Loading";
import AddCircleRoundedIcon from "@mui/icons-material/AddCircleRounded";
import Footer from "../../Shared/Footer/Footer";
import 'react-confirm-alert/src/react-confirm-alert.css';
import ManageEmployeeItem from "../ManageEmployeeItem/ManageEmployeeItem";
import StyledTableCell from "../../../StyledComponents/StyledTableCell";
import StyledTableRow from "../../../StyledComponents/StyledTableRow";


const ManageEmployees = () => {
    const [employees, setEmployees] = useState([]);
    const baseUrl = process.env.REACT_APP_BASE_URL;

    // Get Employee Data
    const employeeUrl = `${baseUrl}/employees`;

    useEffect(() => {
        axios.get(employeeUrl)
            .then((res) => {
                setEmployees(res?.data)
            });
    }, []);

    // Delete an Employee from Database
    const handleDeleteEmployee = (id) => {
        confirmAlert({
            title: 'Confirm to delete employee',
            message: 'Are you sure you want to delete employee?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        axios.delete(`${baseUrl}/employee/${id}`)
                            .then((res) => {
                                const remainingEmployee = employees.filter(
                                    (employee) => employee._id !== id
                                );
                                setEmployees(remainingEmployee);
                                toast.success('Employee Deleted Successfully!', {
                                    position: "top-right"
                                });
                                // window.location.reload(false);
                            })
                            .catch((err) => {
                                toast.error(err);
                            })
                    },
                },
                {
                    label: 'No',
                    onClick: () => '',
                },
            ],
        });
    };

    const navigate = useNavigate();

    return (
        <Grid>
            <PageTitle title = {"Manage Employee"} />
            <Header />
            <CssBaseline />
            <Box component = "main"
                 sx = {{
                     flexGrow: 1, bgcolor: '#F2F4F8', mt: '100px', maxWidth: 1450, margin: '0 auto'
                 }}>

                {/*Table Begin*/}
                <Grid className = 'table-bar'>
                    <Typography variant = "h1"
                                fontSize = {30}
                                fontWeight = {"bold"}
                                color = "#2c425d"
                                sx = {{ padding: "1rem 0" }}
                    >
                        Manage All Employees
                    </Typography>

                </Grid>

                <TableContainer>
                    {!employees.length ? (
                        <Loading height = {'50vh'}></Loading>
                    ) : (
                        <Table responsive="sm" aria-label = "customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell>#</StyledTableCell>
                                    <StyledTableCell>Name</StyledTableCell>
                                    <StyledTableCell align = "left">Image</StyledTableCell>
                                    <StyledTableCell align = "left">Position</StyledTableCell>
                                    <StyledTableCell align = "left">Email</StyledTableCell>
                                    <StyledTableCell align = "left">Gender</StyledTableCell>
                                    <StyledTableCell align = "left">Mobile</StyledTableCell>
                                    <StyledTableCell align = "left">Emergency Contact</StyledTableCell>
                                    <StyledTableCell align = "left">Joining Date</StyledTableCell>
                                    <StyledTableCell align = "left">Social Security</StyledTableCell>
                                    <StyledTableCell align = "right">Manage</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    employees?.length > 0 && employees?.map((employee, index) => (
                                        <StyledTableRow hover
                                                        key = {employee._id}
                                        >
                                            <ManageEmployeeItem
                                                index = {index}
                                                employee = {employee}
                                                deleteEmployee={handleDeleteEmployee}
                                            />
                                        </StyledTableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    )}
                </TableContainer>

                {/*Add Employee Button*/}
                <Grid display = "flex" justifyContent = "space-between" mt = '25px' mb = '5rem'>
                    <Button sx = {{ padding: ".5rem 1.5rem" }} className = "add-employee-btn"
                            onClick = {() => {
                                navigate('/add-employee')
                            }}
                    >
                        <AddCircleRoundedIcon sx = {{ marginRight: '.8rem' }} />
                        Add Employee
                    </Button>
                </Grid>
            </Box>
            <Footer />
        </Grid>
    );
};

export default ManageEmployees;
