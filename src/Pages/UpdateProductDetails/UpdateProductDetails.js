import React, { useEffect, useState } from 'react';
import "./UpdateProductDetails.css";
import axiosPrivate from "../../api/axiosPrivate";
import { signOut } from "firebase/auth";
import auth from "../../firebase.init";
import toast from "react-hot-toast";
import { useForm } from "react-hook-form";
import { useAuthState } from "react-firebase-hooks/auth";
import { useNavigate, useParams } from "react-router-dom";
import Header from "../Shared/Header/Header";
import PageTitle from "../Shared/PageTitle/PageTitle";
import { Button, Container, Form } from "react-bootstrap";
import useItems from "../../hooks/useItems";

const UpdateProductDetails = () => {
    const { productId } = useParams();
    const { register, handleSubmit } = useForm();
    const [brands, setBrands] = useState([]);
    const [suppliers, setSuppliers] = useState([]);
    const [categories, setCategories] = useState([]);
    const [user] = useAuthState(auth);
    const navigate = useNavigate();
    const baseUrl = process.env.REACT_APP_BASE_URL;

    //Product Details Request
    const [{
        name,
        brand,
        description,
        category,
        buyingPrice,
        sellingPrice,
        stock,
        supplier,
        image
    }] = useItems(`${baseUrl}/item/${productId}`);
    // console.log(name);

    const shortName = name?.slice(0, 16);

    //Brand Name Request
    useEffect(() => {
        const getBrands = async () => {
            const url = `${baseUrl}/brand`;
            try {
                const { data } = await axiosPrivate.get(url);
                setBrands(data);
            } catch (error) {
                if (error.response.status === 401 || error.response.status === 403) {
                    await signOut(auth);
                    navigate('/login');
                }
            }
        };
        getBrands();
    }, [user]);

    //Category Request
    useEffect(() => {
        const getCategory = async () => {
            const url = `${baseUrl}/category`;
            try {
                const { data } = await axiosPrivate.get(url);
                setCategories(data);
            } catch (error) {
                if (error.response.status === 401 || error.response.status === 403) {
                    await signOut(auth);
                    navigate('/login');
                }
            }
        };
        getCategory();
    }, [user]);

    //Supplier Name Request
    useEffect(() => {
        const getSuppliers = async () => {
            const url = `${baseUrl}/supplier`;
            try {
                const { data } = await axiosPrivate.get(url);
                setSuppliers(data);
            } catch (error) {
                if (error.response.status === 401 || error.response.status === 403) {
                    await signOut(auth);
                    navigate('/login');
                }
            }
        };
        getSuppliers();
    }, [user]);

    //Data Validation
    const dataValidation = (itemData) => {
        if (itemData?.buyingPrice <= 0) {
            toast.error('Buying price Can\'t be Negative/Zero', {
                position: "top-center"
            });
            return true;
        } else if (itemData?.sellingPrice <= 0) {
            toast.error('Selling price Can\'t be Negative/Zero', {
                position: "top-center"
            });
            return true;
        } else if (itemData?.stock < 0) {
            toast.error('Stock Can\'t be Negative Number', {
                position: "top-center"
            });
            return true;
        } else {
            return false;
        }
    };

    const onSubmit = data => {
        const temp = {...data}
        temp.stock = parseInt(temp?.stock);
        temp.sellingPrice = parseInt(temp?.sellingPrice);
        temp.buyingPrice = parseInt(temp?.buyingPrice);
        const isVerified = dataValidation(temp);
        console.log({ data},{temp})

        if (!isVerified) {
            const url = `${baseUrl}/product/${productId}`;
            fetch(url, {
                method: "PUT",
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(temp)
            })
                .then(res => res.json())
                .then(result => {
                    if (result?.modifiedCount === 1) {
                        toast.success('Your Item has Been Updated');
                    }
                });
        }
    };
    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container w-50 mx-auto">
                <PageTitle title = {"Update Product"}></PageTitle>

                <Container>
                    <Form className = "update-item-border" onSubmit = {handleSubmit(onSubmit)}>
                        <h4 className = " title-color text-center mb-4"
                            style = {{
                                fontWeight: "bold",
                                marginTop: "1rem"
                            }}>{`Update ` + shortName + ' Details'}</h4>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control type = "text" defaultValue = {name}
                                          placeholder = "Product Name" {...register("name", {
                                required: true
                            })} />

                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 forms-control">
                            <Form.Group className="col-lg forms-control">
                                <Form.Label>Brand</Form.Label>
                                <Form.Select id = "brandList" {...register("brand", {
                                    required: true
                                })}>
                                    <option>{brand}</option>
                                    {
                                        brands?.map(brand => <option
                                            key = {brand._id}
                                        >{brand.name}
                                        </option>)
                                    }
                                </Form.Select>
                            </Form.Group>

                            <Form.Group  style = {{ marginLeft: "1rem" }} className="col-lg forms-control">
                                <Form.Label>Category</Form.Label>
                                <Form.Select id = "categoryList" {...register("category", {
                                    required: true
                                })}>
                                    <option>{category}</option>
                                    {
                                        categories?.map(category => <option
                                            key = {category._id}
                                        >{category.name}
                                        </option>)
                                    }
                                </Form.Select>
                            </Form.Group>
                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 forms-control">

                            <Form.Group className="col-lg forms-control">
                                <Form.Label>Buying Price</Form.Label>
                                <Form.Control type = "number" defaultValue = {buyingPrice}
                                              placeholder = "Buying Price" {...register("buyingPrice", {
                                    required: true,
                                    maxLength: 20
                                })} />
                            </Form.Group>

                            <Form.Group  style = {{ marginLeft: "1rem" }} className="col-lg forms-control">
                            <Form.Label>Selling Price</Form.Label>
                            <Form.Control type = "number" defaultValue = {sellingPrice}
                                          placeholder = "Selling Price" {...register("sellingPrice", {
                                required: true,
                                maxLength: 20
                            })} />
                            </Form.Group>

                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "formBasicStock">
                            <Form.Label>Stock</Form.Label>
                            <Form.Control type = "number" defaultValue = {stock === null ? 0 : stock}
                                          placeholder = "Stock" {...register("stock", {
                                required: true,
                                maxLength: 20
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "formBasicSupplier">
                            <Form.Label>Supplier</Form.Label>
                            <Form.Select id = "suppliersList"
                                         {...register("supplier", {
                                             required: true
                                         })}>
                                <option>{supplier}</option>
                                {
                                    suppliers?.map(supplier => <option
                                        key = {supplier._id}
                                    >{supplier.name}
                                    </option>)
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "formBasicEmail">
                            <Form.Control type = "email"
                                          readOnly
                                          value = {user?.email}
                                          {...register("email")} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control placeholder = "Description" as = "textarea" defaultValue = {description}
                                          rows = {3} {...register("description", {
                                required: true
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "imageUrl">
                            <Form.Label>Image Link</Form.Label>
                            <Form.Control type = "text" defaultValue = {image}
                                          placeholder = "Enter Image URL" {...register("image", {
                                required: true,
                            })} />
                        </Form.Group>

                        <Button className = "update-item-btn" style = {{ width: "60%" }}
                                variant = "primary mx-auto d-block mb-5" type = "submit">
                            Update Product
                        </Button>
                    </Form>
                </Container>
            </div>
        </>
    );
};

export default UpdateProductDetails;
