import Header from "../Shared/Header/Header";
import PageTitle from "../Shared/PageTitle/PageTitle";
import { Button, Container, Form } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import useItems from "../../hooks/useItems";
import toast from "react-hot-toast";

const UpdateClientDetails = () => {
    const { clientId } = useParams();
    const { register, handleSubmit } = useForm();
    const navigate = useNavigate();
    const baseUrl = process.env.REACT_APP_BASE_URL;

    //Get Client data using useItem Hook
    const [{
        clientName,
        clientEmail,
        clientCompanyName,
        clientContactNumber,
        clientAddress
    }] = useItems(`${baseUrl}/client/${clientId}`);
    console.log(clientContactNumber)

    const onSubmit = data => {
        console.log({ data })
        const url = `${baseUrl}/client/${clientId}`;
        fetch(url, {
            method: "PUT",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result?.modifiedCount === 1) {
                    toast.success('Client Information Has Been Updated');
                    navigate("/manage-clients");
                }
            });
    };

    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container w-50 mx-auto">
                <PageTitle title = {"Update Client"}></PageTitle>

                <Container>
                    <Form className = "add-employee-border" onSubmit = {handleSubmit(onSubmit)}>
                        <h4 className = " title-color text-center mb-4"
                            style = {{ fontWeight: "bold", marginTop: "1rem" }}>{`Update ${clientName} Details`}</h4>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text" defaultValue = {clientName}
                                          placeholder = "Name" {...register("clientName", {
                                required: true, maxLength: 20
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "email" defaultValue = {clientEmail}
                                          placeholder = "Email" {...register("clientEmail", {
                                required: true,
                                pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                            })} />
                        </Form.Group>


                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicBrand">
                            <Form.Control type = "text" defaultValue = {clientCompanyName}
                                          placeholder = "Company Name" {...register("clientCompanyName", {
                                required: true
                            })} />
                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicPrice">
                            <Form.Control type = "number" defaultValue = {clientContactNumber}
                                          placeholder = "Contact Number" {...register("clientContactNumber", {
                                required: true,
                                minLength: 8,
                                maxLength: 12
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "description">
                            <Form.Control placeholder = "Address" as = "textarea" defaultValue = {clientAddress}
                                          rows = {3} {...register("clientAddress", {
                                required: true
                            })} />
                        </Form.Group>

                        <Button className = "add-btn" style = {{ width: "60%" }}
                                variant = "primary mx-auto d-block mb-5" type = "submit">
                            Update Client
                        </Button>
                    </Form>
                </Container>
            </div>
        </>
    );
};

export default UpdateClientDetails;
