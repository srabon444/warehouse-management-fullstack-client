import React from 'react';
import "./Banner.css";
import { Grid } from "@mui/material";
import banner from "../../../images/banner-img-transparent.png";
import Typography from "@mui/material/Typography";

const Banner = () => {
    return (
        <Grid container direction="row" sx={{ padding:"2rem 8rem 1.5rem 8rem"}}
              justifyContent="center"
              alignItems="center" columnSpacing={4}>
            <Grid item xs={12} md={6} className = "banner-content" sx={{paddingRight:"8rem"}}>
                <Typography variant="h1"
                            fontSize={56}
                            fontWeight={"bold"}
                            gutterBottom>
                    Your Inventory is now automated
                </Typography>
                <Typography variant="subtitle1"
                            fontSize={18}
                            gutterBottom>
                    Track and manage your inventory in real-time across multiple channels for your fast growing brands with Shipido, and widen your eCommerce reach.
                </Typography>
            </Grid>
            <Grid item xs={12} md={6}>
                <img width="100%" src = {banner} alt = "Banner Warehouse" />
            </Grid>
        </Grid>
    );
};

export default Banner;
