import React from 'react';
import "./Inventory.css";
import {useNavigate} from "react-router-dom";
import {useQuery} from "react-query";
import axios from "axios";
import Loading from "../../Shared/Loading/Loading";
import {Button, Grid, Stack} from "@mui/material";
import Typography from "@mui/material/Typography";
import InventoryItem from "../InventoryItem/InventoryItem";
import TrendingFlatIcon from '@mui/icons-material/TrendingFlat';
import PageTitle from "../../Shared/PageTitle/PageTitle";

const Inventory = () => {
  const navigate = useNavigate();
  const baseUrl = process.env.REACT_APP_BASE_URL;

  //Get Inventory Data Using Custom Hook
  const {data, isLoading} = useQuery('items', () => axios.get(`${baseUrl}/items`));

  if (isLoading) {
    return <Loading/>
  }

  const items = data?.data.length > 0 ? data?.data?.slice(0, 6) : [];

  return (
      <Grid container sx={{padding: "0rem 1rem 4rem 8rem"}}>
        <PageTitle title={"Inventory"}></PageTitle>
        <Grid>
          <Typography variant="h1"
                      fontSize={45}
                      fontWeight={"bold"}
                      color="#2c425d"
                      sx={{padding: "2rem 0"}}
          >
            My Inventory
          </Typography>
          {
            !items?.length ? (
                <Loading height={'60vh'}></Loading>
            ) : (
                <Grid container
                      direction="row"
                      justifyContent="center"
                      alignItems="center"
                >
                  {
                    items?.map((item) => (
                        <Grid item sx={{paddingRight: "0.5rem", marginBottom: '2.5rem'}}
                              key={item._id}
                              xs={12} sm={6} md={4}
                        >
                          <InventoryItem
                              item={item}
                          ></InventoryItem>
                        </Grid>
                    ))
                  }
                </Grid>
            )
            //    xs, extra-small: 0px
            // sm, small: 600px
            // md, medium: 900px
            // lg, large: 1200px
            // xl, extra-large: 1536px
          }
        </Grid>
        {/*Manage all Inventories Button Begin*/}
        <Grid item xs={12} pr={{xl: "3.8rem", lg: "0.6rem", md: "0.5rem", sm: "0.4rem"}}>
          <Stack direction="row"
                 justifyContent="flex-end"
                 alignItems="center"
          >
            <Button sx={{padding: ".5rem 1.5rem"}} className="manage-all-inventory-btn"
                    onClick={() => {
                      navigate('/manage-inventory')
                    }}
            >Manage All Inventories
              <TrendingFlatIcon sx={{marginLeft: '.8rem'}}/>
            </Button>
          </Stack>
        </Grid>
      </Grid>
  );
};

export default Inventory;
