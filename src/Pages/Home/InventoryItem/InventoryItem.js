import React from 'react';
import "./InventoryItem.css";
import { useNavigate } from "react-router-dom";
import { Badge, Box, Button, Chip, Grid } from "@mui/material";
import Typography from "@mui/material/Typography";
import LaptopChromebookIcon from '@mui/icons-material/LaptopChromebook';
import DesktopWindowsIcon from '@mui/icons-material/DesktopWindows';
import MonitorIcon from '@mui/icons-material/Monitor';
import CategoryRoundedIcon from '@mui/icons-material/CategoryRounded';

const InventoryItem = ({ item }) => {
    const { _id, name, description, sellingPrice, stock, supplier, image, category } = item;

    // Comma seperated price
    const priceWithComma = sellingPrice?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    //short name & description for homepage
    const shortName = name.slice(0, 20);
    const shortDescription = description.slice(0, 120);

    const navigate = useNavigate();

    return (
        // Card Begin
        <Grid item sx = {{ background: 'white', borderRadius: '3px', boxShadow: '5' }} height = "32rem"
              maxWidth = "22rem"
              container direction = "row" justifyContent = "center" alignItems = "flex-end">
            <Grid item>
                {/*Image Area*/}
                <Grid item margin = "0 auto" width = "70%">
                    <Grid item
                          ml = {{ xl: "-3.36rem", lg: "-3.36rem", md: "-2.15rem", sm: "-1.99rem", xs: "-1.79rem" }}>
                        <Typography className = "vendor-name-mark"
                                    variant = "subtitle2">Supplier: {supplier}
                        </Typography>
                    </Grid>

                    <img width = "100%" height = "100%" src = {image} alt = {name} />
                </Grid>
                {/*Content Area*/}
                <Grid item sx = {{ margin: '0 1rem' }}>
                    <Typography variant = "h5">{shortName}</Typography>
                    {/*Price & Stock Begin*/}
                    <Grid item display = "flex" justifyContent = "space-between" margin = ".7rem 0 1rem 0">
                        <Typography variant = "h6">${priceWithComma}</Typography>
                        {stock ?
                            <Badge sx = {{ marginRight: '1rem' }} color = "success" badgeContent = {stock}>
                                {
                                    category?.toLowerCase() === "laptop" ? <LaptopChromebookIcon /> :
                                        category?.toLowerCase() === "monitor" ?
                                            <DesktopWindowsIcon /> :
                                            category?.toLowerCase() === "pc" ?
                                                <MonitorIcon /> :
                                                <CategoryRoundedIcon />

                                }
                            </Badge>
                            :
                            <Chip
                                avatar =
                                    {
                                        category?.toLowerCase() === "laptop" ?
                                            <LaptopChromebookIcon className = "out-stock-chip-color" /> :
                                            category?.toLowerCase() === "monitor" ?
                                                <DesktopWindowsIcon /> :
                                                category?.toLowerCase() === "pc" ?
                                                    <MonitorIcon /> :
                                                    <CategoryRoundedIcon />

                                    }
                                sx = {{ fontWeight: 'bold' }}
                                label = "Sold Out"
                                variant = "outlined"
                                color = 'error'
                            />
                        }
                    </Grid>
                    {/*Price & Stock End*/}
                    <Box component = "span">
                        <Typography variant = "body2" title = {description}>{shortDescription}...</Typography>
                    </Box>
                </Grid>
            </Grid>
            {/*Manage Stock Button*/}
            <Grid item xs = {12}>
                <Button
                    variant = "contained"
                    className = "manage-stock-btn"
                    onClick = {() => navigate(`/inventory/stock-update/${_id}`)}
                >Manage Stock</Button>
            </Grid>
        </Grid>
        //    Card End
    );
};

export default InventoryItem;
