import React from 'react';
import Footer from "../../Shared/Footer/Footer";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import Header from "../../Shared/Header/Header";
import "./Home.css";
import { Box } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import Banner from "../Banner/Banner";
import Inventory from "../Inventory/Inventory";


const Home = () => {
    return (

        <Box sx={{ display: 'flex', backgroundColor:'white' }}>
            <PageTitle title={"Home"}/>
            <CssBaseline />
            <Box
                component="main"
            >
                <Header />
                <Banner/>
                <Inventory/>
                <Footer/>
            </Box>
        </Box>
    );
};

export default Home;
