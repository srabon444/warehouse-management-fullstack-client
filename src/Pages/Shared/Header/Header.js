import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import Link from '@mui/material/Link';
import "./Header.css";
import { Box, Grid, Stack } from "@mui/material";
import { useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import auth from "../../../firebase.init";
import { signOut } from "firebase/auth";
import LogoutIcon from '@mui/icons-material/Logout';
import SideDrawer from "../../Drawer/SideDrawer";

const navItem = [
    {
        text: 'Home',
        to: '/',
        href: '/'
    },
    {
        text: 'Dashboard',
        to: '/dashboard',
        href: '/dashboard'
    },
    {
        text: 'Employees',
        to: '/manage-employee',
        href: '/manage-employee'
    },
    {
        text: 'Add Items',
        to: '/add-item',
        href: '/add-item'
    },
    {
        text: 'About',
        to: '/about',
        href: '/about'
    },
];


const Header = () => {
    const [user] = useAuthState(auth);
    const [open, setOpen] = useState(false);

    const handleSignOut = () => {
        signOut(auth);
    };

    // console.log("From Header Login/Logout: ", user);

    return (
        <Box sx = {{ display: 'flex' }}>
            <AppBar component = "nav"
                    position = "sticky"
                    color = "default"
                    elevation = {0}
                    sx = {{
                        borderBottom: (theme) => `1px solid ${theme.palette.divider}`,
                        bgcolor: "white", flexWrap: 'wrap', overflow: "hidden"
                    }}
            >

                <Stack
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                    spacing={2}
                    sx={{marginLeft:'3.5rem'}}
                >
                    <SideDrawer/>
                    <Grid
                        container
                        direction = "row"
                        justifyContent = "flex-end"
                        alignItems = "center"
                    >

                        <nav
                            style = {{ flexWrap: 'wrap' }}
                            className = {!open ? 'navigation' : ' nav-mobile'}
                        >
                            {
                                navItem?.map((item, index) => (
                                    <Link
                                        key = {item.text}
                                        className = "link-hover"
                                        variant = "button"
                                        color = "text.primary"
                                        href = {item.href}
                                        // to = {item.to}
                                        sx = {{ my: 1, mx: 1.5, textDecoration: 'none' }}
                                    >{item.text}</Link>
                                ))
                            }
                            {!user?.email ?
                                <Button variant = "contained" className = "login-btn" href = "/login"
                                        sx = {{ my: 1, mx: 1.5 }}>
                                    Login
                                </Button>
                                :
                                <>
                                    <div style={{fontWeight:"bold", }}>
                                        {
                                            user?.displayName
                                        }
                                    </div>
                                    <Button onClick = {handleSignOut} variant = "contained" className = "logout-btn"
                                            sx = {{ my: 1, mx: 1.5 }}><LogoutIcon
                                        sx = {{ padding: "2px" }} />LogOut</Button>
                                </>
                            }

                        </nav>
                    </Grid>
                </Stack>

            </AppBar>
        </Box>
    );
}

export default Header;