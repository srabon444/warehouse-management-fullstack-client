import React from 'react';
import notFound from "../../../images/notFound.jpg";
import { useNavigate } from "react-router-dom";
import { Button, Grid } from "@mui/material";
import "./NotFound.css";

const NotFound = () => {
    const navigate = useNavigate();
    const returnhome = () => {
        navigate("/");
    };

    return (
        <>
            <Grid sx={{backgroundColor:'white', textAlign:'center', marginTop:'3rem'}}>
                <img className = "background" src = {notFound} alt = "" />
                <Grid container
                      direction = "column"
                      justifyContent = "center"
                      alignItems = "center"
                      alignSelf = "center">
                    <Button className="not-found-btn" variant = "contained" onClick = {returnhome}>Return Home</Button>
                </Grid>
            </Grid>
        </>
    );
};

export default NotFound;
