import React from 'react';
import { faApple, faGooglePlay } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import "./Footer.css";
import { Grid } from "@mui/material";


function Copyright(props) {
    return (
        <Link style = {{ textDecoration: 'none' }} color = "inherit" href = "https://gitlab.com/srabon444">
            <Typography variant = "body2" color = "text.secondary" align = "center" {...props}>
                {'Copyright © '} {new Date().getFullYear()} {" | Shipido all rights reserved."}
            </Typography>
        </Link>
    );
}

const Footer = () => {
    return (
        // Begin Footer
        <Grid container rowSpacing = {5} className = "footer-top">
            <Grid item md = {6} xs = {12} sx = {{ paddingRight: "100px" }}>
                <h3>About Us</h3>
                <p>Every month, millions of sales orders flow through our platform - reducing cost, effort and
                    time for product sellers so they can quickly get products to customers and build their
                    brands, without worrying about their operations</p>
            </Grid>

            <Grid item xs = {12} md = {2} className = "footer-column column-2">
                <h3>Useful Links</h3>
                <div>
                    <Link to = "/">Home</Link>
                    <Link to = "/about">About</Link>
                    <Link to = "/blog">Blog</Link>
                    <Link to = "/manage-inventory">Inventory</Link>
                </div>
            </Grid>

            <Grid item md = {4} xs = {12} className = "footer-column">
                <h3>Join Our Community</h3>
                <div className = "mobile-app">
                    {/*Apple App Store Logo*/}
                    <div className = "app">
                        <FontAwesomeIcon
                            icon = {faApple}
                        ></FontAwesomeIcon>
                        <p>Download on <br />
                            <b>App Store</b>
                        </p>
                    </div>

                    {/*Google Play Store Logo*/}
                    <div className = "app">
                        <FontAwesomeIcon
                            icon = {faGooglePlay}
                        ></FontAwesomeIcon>
                        <p>Download on <br />
                            <b>Play Store</b>
                        </p>
                    </div>
                </div>

            </Grid>

            <Grid item xs = {12}>
                <Copyright />
            </Grid>
        </Grid>

        // End Footer
    );
};

export default Footer;
