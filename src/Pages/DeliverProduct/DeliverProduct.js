import React, { useEffect, useState } from 'react';
import "./DeliverProduct.css";
import { Autocomplete, Button, Chip, Container, Grid, Stack, TextField, Tooltip } from "@mui/material";
import PageTitle from "../Shared/PageTitle/PageTitle";
import Header from "../Shared/Header/Header";
import Typography from "@mui/material/Typography";
import { useNavigate, useParams } from "react-router-dom";
import useItems from "../../hooks/useItems";
import toast from "react-hot-toast";
import axios from "axios";
import { Controller, useForm } from "react-hook-form";
import Loading from "../Shared/Loading/Loading";
import TrendingFlatIcon from "@mui/icons-material/TrendingFlat";
import Footer from "../Shared/Footer/Footer";
import Inventory2RoundedIcon from '@mui/icons-material/Inventory2Rounded';
import LocalMallRoundedIcon from '@mui/icons-material/LocalMallRounded';
import SellRoundedIcon from '@mui/icons-material/SellRounded';

const actionTypes ={
    productDelivery:"product-delivery",
    stockUpdate:"stock-update"
}

const DeliverProduct = () => {
    const { itemId, actionType } = useParams();
    const navigate = useNavigate();
    const [itemStock, setItemStock] = useState(0);
    const [selectClient, setSelectClient] = useState(null);
    const baseUrl = process.env.REACT_APP_BASE_URL;

    //Get Clients data using useItem Hook
    const [
        clients
    ] = useItems(`${baseUrl}/clients`);

    //Get Item data using useItem Hook
    const [{
        name,
        description,
        category,
        buyingPrice,
        sellingPrice,
        stock,
        supplier,
        image
    }] = useItems(`${baseUrl}/item/${itemId}`);

    const buyingPriceWithComma = buyingPrice
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    const sellingPriceWithComma = sellingPrice
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    const shortName = name?.slice(0, 16);

    useEffect((e) => {
        setItemStock(stock);
    }, [stock]);

    //react hook form
    const { control, handleSubmit, reset } = useForm();

    //Data Validation
    const dataValidation = (itemData, actionType) => {
        console.log("Item data" + itemData)
        console.log("Item Stock" + itemStock)

        if (itemData <= 0) {
            toast.error('Stock Number Can\'t be Negative/Zero', {
                position: "top-center"
            });
            return true;
        } else if (actionType === "deliveryItem" && (itemStock - itemData) < 0) {
            toast.error('You don\'t have enough stock', {
                position: "top-center"
            });
            return true;
        } else if (actionType === "deliveryItem" && (selectClient === null || selectClient?.clientCompanyName === "")) {
            toast.error('Please Select Client', {
                position: "top-center"
            });
            return true;
        } else if (isNaN(itemData)) {
            toast.error('Stock Can\'t be empty', {
                position: "top-center"
            });
            return true;
        } else {
            return false;
        }
    };

    //Handles remove an item from stock
    const handleItemDeliver = (data) => {
        const temp = { ...data }
        temp.deliverItem = parseInt(temp?.deliverItem);

        const isVerified = dataValidation(temp?.deliverItem, "deliveryItem");
        if (!isVerified) {
            if (itemStock) {
                const updateItemsStock = parseInt(itemStock) - temp.deliverItem;
                setItemStock(updateItemsStock);

                const url = `${baseUrl}/product-delivery/${itemId}`;

                axios?.put(url, {
                    stock: updateItemsStock,
                    client: selectClient,
                    productId: itemId,
                    deliveryAmount: temp.deliverItem
                })
                    .then((res) => {
                        setSelectClient(null);
                        toast.success(`${data.deliverItem} ${category} has been delivered.`);
                    })
                    .catch((err)=>console.log({err}))
                reset();
            }
        }
    };

    //Handles specific items' stock increase to the inventory
    const handleUpdateStock = (data) => {
        const temp = { ...data }
        temp.addToStock = parseInt(temp?.addToStock);

        const isVerified = dataValidation(temp.addToStock, "updateStock");
        // console.log(parseInt(data?.addToStock));

        if (!isVerified) {
            const updatedItemsStock = itemStock + parseInt(temp?.addToStock);
            setItemStock(updatedItemsStock);
            // console.log({ updatedItemsStock });
            const addUrl = `${baseUrl}/stock-update/${itemId}`;

            axios?.put(addUrl, {
                stock: updatedItemsStock
            })
                .then((res) => {
                    toast.success(`${data?.addToStock} ${category}s added to inventory`);
                });
            reset();
        }
    };

    if (!name) {
        return <Loading />
    }

    return (
        <Grid sx = {{ backgroundColor: "white" }}>
            <PageTitle title = {"Item Details"} />
            <Header />
            <Container sx = {{ backgroundColor: "white", padding: "2rem 4rem 1.5rem 4rem", marginTop: "1.8rem" }}
                       className = "item-details-border">
                <Grid container justifyContent = "flex-start">
                    {/*
                        ---> Breakpoints:
                        xs, extra-small: 0px - 599px
                        sm, small: 600px - 899px
                        md, medium: 900px - 1199px
                        lg, large: 1200px - 1535px
                        xl, extra-large: 1536px

                    */}
                    <Grid item sm = {12} md = {7} lg = {7}>
                        <Typography
                            variant = "p"
                            sx = {{ marginLeft: "3rem" }}
                            className = "item-content"
                        >
                            Supplier: <span style = {{ fontWeight: "bold" }}>{supplier}</span>
                        </Typography>
                        <Tooltip title = {name}>
                            <Typography variant = "h2"
                                        fontSize = {40}
                                        fontWeight = "bold"
                                        sx = {{ marginLeft: "3rem" }}
                                        className = "item-content"
                            >{shortName}
                            </Typography>
                        </Tooltip>
                        <img style = {{ marginLeft: "2rem" }} src = {image} width = "50%" alt = {shortName} />

                    </Grid>

                    <Grid ml = {{ xl: "3rem", lg: "2rem", md: "2rem", sm: "2rem", xs: "2rem" }} item sm = {8}
                          md = {4} lg = {4}>
                        <Stack direction = "column" className = "price-design">
                            <Stack direction = "row" alignItems = "center" sx = {{ marginBottom: "4px" }}>
                                <LocalMallRoundedIcon />
                                <Typography variant = "body1"
                                            sx = {{ marginLeft: ".8rem" }}
                                >Buying Price: <span
                                    style = {{
                                        fontWeight: "bold",
                                        marginLeft: ".4rem"
                                    }}>${buyingPriceWithComma}</span></Typography>
                            </Stack>
                            <Stack direction = "row" alignItems = "center">
                                <SellRoundedIcon />
                                <Typography variant = "body1"
                                            sx = {{ marginLeft: ".8rem" }}
                                >Selling Price: <span
                                    style = {{
                                        fontWeight: "bold",
                                        marginLeft: ".4rem"
                                    }}>${sellingPriceWithComma}</span></Typography>
                            </Stack>
                        </Stack>

                        <Stack direction = "row" alignItems = "center" className = "stock-design">
                            <Inventory2RoundedIcon />
                            <Typography variant = "h6" sx = {{ marginLeft: ".8rem" }}><span
                                style = {{ marginRight: ".4rem" }}>Stock:</span>
                                {
                                    itemStock ? itemStock :
                                        <Chip
                                            sx = {{ fontWeight: 'bold' }}
                                            label = "Sold Out"
                                            variant = "outlined"
                                            color = 'error'
                                        />

                                }</Typography>
                        </Stack>

                        {/*// add to stock area*/}
                        {  actionType === actionTypes.stockUpdate ? <form>
                            <Stack direction = "column" className = "add-to-stock-box">
                                <Controller
                                    name = {"addToStock"}
                                    control = {control}
                                    render = {({ field: { onChange, value } }) => (
                                        <TextField
                                            onChange = {onChange}
                                            value = {value || ''}
                                            size = "small"
                                            id = "outlined-basic"
                                            label = "Enter Quantity"
                                            variant = "filled"
                                            type = "number"
                                            className = "add-to-stock-box-input"
                                            required
                                        />
                                    )}
                                />
                                <Button
                                    onClick = {handleSubmit(handleUpdateStock)}
                                    variant = "contained"
                                    className = "add-to-stock-box-btn"
                                >Add to Stock</Button>
                            </Stack>
                        </form>
                        // product delivery area
                        :<form>
                            <Stack direction = "column" className = "add-to-stock-box">
                                <Autocomplete
                                    id = "controlled-demo"
                                    value = {selectClient}
                                    options = {clients}
                                    getOptionLabel = {(option) => option?.clientCompanyName}
                                    onChange = {(event, newValue) => {
                                        setSelectClient(newValue)
                                    }}
                                    renderInput = {(params) =>
                                        <TextField {...params}
                                                   label = "Client Name"
                                                   variant = "filled"
                                                   className = "add-to-stock-box-input"
                                        />}
                                />

                                <Controller
                                    name = {"deliverItem"}
                                    control = {control}
                                    render = {({ field: { onChange, value } }) => (
                                        <TextField
                                            onChange = {onChange}
                                            value = {value || ''}
                                            size = "small"
                                            id = "outlined-basic"
                                            label = "Enter Quantity"
                                            variant = "filled"
                                            type = "number"
                                            className = "add-to-stock-box-input"
                                            required
                                        />
                                    )}
                                />

                                <Button
                                    onClick = {handleSubmit(handleItemDeliver)}
                                    variant = "contained"
                                    className = "add-to-stock-box-btn"
                                >Deliver Product</Button>
                            </Stack>
                        </form>}

                        {/*Manage All Inventories button Begin*/}
                        <Stack>
                            <Button sx = {{ padding: ".5rem 1.5rem" }}
                                    className = "manage-all-inventory-btn"
                                    onClick = {() => {
                                        navigate('/manage-inventory')
                                    }}
                            >Manage All Inventories
                                <TrendingFlatIcon sx = {{ marginLeft: '.8rem' }} />
                            </Button>
                        </Stack>
                        {/*Manage All Inventories button End*/}
                    </Grid>
                </Grid>

                <Grid sx = {{ marginLeft: "2rem", marginRight: "2rem", marginTop: "2rem" }}
                      className = "item-content"
                >
                    <Typography variant = "body1">
                        <span style = {{ fontWeight: "bold" }}>Additional Details: </span>
                    </Typography>
                    <Typography
                        variant = "subtitle1"
                        sx = {{ marginTop: "1rem" }}
                    >{description}</Typography>
                </Grid>
            </Container>
            <Footer />
        </Grid>
    );
};

export default DeliverProduct;
