import React from 'react';
import "./ManageDeliveryItem.css";
import StyledTableCell from "../../../StyledComponents/StyledTableCell";
import { Tooltip } from "@mui/material";
import VisibilityRoundedIcon from '@mui/icons-material/VisibilityRounded';
import { useNavigate } from "react-router-dom";

const ManageDeliveryItem = ({ item, index }) => {
    const { _id, client, product, deliveryAmount } = item;
    const navigate = useNavigate();

    // Short Name
    const shortName = product?.name?.slice(0, 13);

    const sellingPriceWithComma = product?.sellingPrice
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return (
        <>
            <StyledTableCell sx = {{ fontWeight: 'bold' }} align = "left">
                {index + 1}.</StyledTableCell>
            <StyledTableCell align = "left">{_id}</StyledTableCell>
            <Tooltip title = {product?.name} arrow>
                <StyledTableCell sx = {{ fontWeight: 'bold' }} scope = "row">{shortName}...</StyledTableCell>
            </Tooltip>
            {/*<StyledTableCell width = "2rem" align = "right">*/}
            {/*    <img width = "100%" src = {image} alt = "" />*/}
            {/*</StyledTableCell>*/}
            <StyledTableCell align = "left">{product?.brand}</StyledTableCell>
            <StyledTableCell align = "left">{product?.category}</StyledTableCell>
            <StyledTableCell align = "left">${sellingPriceWithComma}</StyledTableCell>
            <StyledTableCell align = "left">{client?.clientCompanyName}</StyledTableCell>
            <StyledTableCell align = "left">{client?.clientContactNumber}</StyledTableCell>
            <StyledTableCell align = "center">{deliveryAmount}</StyledTableCell>
            <StyledTableCell align = "center">
                <Tooltip title = "See Details">
                    <VisibilityRoundedIcon color = "secondary"
                                               sx = {{ cursor: 'pointer' }}
                                               onClick = {() => navigate(`/delivery-details/${_id}`)}
                    />
                </Tooltip>
            </StyledTableCell>
        </>
    );
};

export default ManageDeliveryItem;
