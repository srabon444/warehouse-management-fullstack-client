import React, { useEffect, useState } from 'react';
import "./ManageDelivery.css";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import Header from "../../Shared/Header/Header";
import CssBaseline from "@mui/material/CssBaseline";
import {
    Box,
    Grid,
    Table, TableBody,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Loading from "../../Shared/Loading/Loading";
import StyledTableCell from "../../../StyledComponents/StyledTableCell";
import Footer from "../../Shared/Footer/Footer";
import axios from "axios";
import StyledTableRow from "../../../StyledComponents/StyledTableRow";
import ManageDeliveryItem from "../ManageDeliveryitem/ManageDeliveryItem";

const ManageDelivery = () => {
    const [items, setItems] = useState([]);
    const baseUrl = process.env.REACT_APP_BASE_URL;

    const deliveryUrl = `${baseUrl}/deliveries`;

    useEffect(() => {
        axios.get(deliveryUrl)
            .then((res) => {
                setItems(res?.data)
                // console.log("hit")
                console.log(items);
            });
    }, []);

    return (
        <Grid>
            <PageTitle title = {"Manage Deliveries"} />
            <Header />
            <CssBaseline />
            <Box component = "main"
                 sx = {{
                     flexGrow: 1, bgcolor: '#F2F4F8', mt: '100px', maxWidth: 1400, margin: '0 auto'
                 }}>

                {/*Table Begin*/}
                <Grid className = 'table-bar'>
                    <Typography variant = "h1"
                                fontSize = {30}
                                fontWeight = {"bold"}
                                color = "#2c425d"
                                sx = {{ padding: "1rem 0" }}
                    >
                        Manage All Deliveries
                    </Typography>
                </Grid>

                <TableContainer>
                    {!items?.length ? (
                        <Loading height = {'50vh'}></Loading>
                    ) : (
                        <Table responsive = "sm" aria-label = "customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell>#</StyledTableCell>
                                    <StyledTableCell>Delivery ID</StyledTableCell>
                                    <StyledTableCell align = "left">Product Name</StyledTableCell>
                                    <StyledTableCell align = "left">Brand</StyledTableCell>
                                    <StyledTableCell align = "left">Category</StyledTableCell>
                                    <StyledTableCell align = "left">Selling Price</StyledTableCell>
                                    <StyledTableCell align = "left">Client</StyledTableCell>
                                    <StyledTableCell align = "left">Contact</StyledTableCell>
                                    <StyledTableCell align = "center">Amount</StyledTableCell>
                                    <StyledTableCell align = "center">Action</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    items?.length > 0 && items?.map((item, index) => (
                                        <StyledTableRow hover
                                                        key = {item._id}
                                        >
                                            <ManageDeliveryItem
                                                index = {index}
                                                item = {item}
                                            />
                                        </StyledTableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    )}
                </TableContainer>
            </Box>
            <Footer />
        </Grid>
    );
};

export default ManageDelivery;
