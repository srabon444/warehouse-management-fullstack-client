import React, { useEffect, useRef, useState } from 'react';
import "./DeliveryReport.css";
import { useNavigate, useParams } from "react-router-dom";
import PageTitle from "../Shared/PageTitle/PageTitle";
import Header from "../Shared/Header/Header";
import CssBaseline from "@mui/material/CssBaseline";
import { Box, Button, Grid, Stack, Table, TableBody, TableContainer, TableHead, TableRow } from "@mui/material";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Loading from "../Shared/Loading/Loading";
import StyledTableCell from "../../StyledComponents/StyledTableCell";
import axios from "axios";
import StyledTableRow from "../../StyledComponents/StyledTableRow";
import { useReactToPrint } from "react-to-print";
import toast from "react-hot-toast";
import PrintIcon from '@mui/icons-material/Print';

const DeliveryReport = () => {
    const [report, setReport] = useState({});
    const { deliveryId } = useParams();
    const baseUrl = process.env.REACT_APP_BASE_URL;
    const navigate = useNavigate();

    const deliveryUrl = `${baseUrl}/delivery-details/${deliveryId}`;

    useEffect(() => {
        axios?.get(deliveryUrl)
            .then((res) => {
                setReport(res?.data[0])
            }).catch((err) => {
            navigate('/not-found');
        })
    }, []);

    const { _id, product, client, deliveryAmount, deliveryDate } = report;

    const slicedDate = deliveryDate?.toString().split('T')[0];

    const sellingPriceWithComma = product?.sellingPrice
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    const totalPrice = product?.sellingPrice * deliveryAmount;
    const totalPriceWithComma = totalPrice
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    const discount = 20;
    const subTotalLessDiscount = totalPrice - (totalPrice * (discount / 100));
    const subTotalLessDiscountWithComma = subTotalLessDiscount?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    const taxRate = 12;
    const totalTax = subTotalLessDiscount * (taxRate / 100);
    const totalTaxWithComma = totalTax?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    const totalFinalPrice = subTotalLessDiscount + totalTax;
    const totalFinalPriceWithComma = totalFinalPrice?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    const componentRef = useRef();
    const handlePrint = useReactToPrint({
        content: () => componentRef.current,
        documentTitle: 'emp-data',
        onAfterPrint: () => toast.success("Print Successful.")
    })
    return (
        <Grid paddingBottom = "8rem">
            <PageTitle title = {"Invoice"} />
            <Header />
            <CssBaseline />

            <Box component = "main"
                 sx = {{
                     flexGrow: 1, bgcolor: '#F2F4F8', mt: '100px', maxWidth: 1000, margin: '0 auto'
                 }}>

                {/*Print Button*/}
                <Grid display = "flex" alignItems = "center" justifyContent = "flex-end" marginTop = "2rem"
                      sx = {{ padding: "0rem 2rem" }}>
                    <Button className = "add-item-btn" variant = "contained" onClick = {handlePrint}
                            startIcon = {<PrintIcon />}>Print</Button>
                </Grid>

                <Box ref = {componentRef} sx = {{ padding: "1rem 2rem" }}>
                    {/*Table Begin*/}
                    <Grid className = 'invoice-bar'>
                        <Stack direction = "row"
                               justifyContent = "space-between"
                               alignItems = "center"
                        >
                            <Typography variant = "h1"
                                        fontSize = {30}
                                        fontWeight = {"bold"}
                                        color = "#2c425d"
                            >
                                Invoice
                            </Typography>

                            <Typography variant = "h1"
                                        fontSize = {30}
                                        fontWeight = {"bold"}
                                        color = "#009B77"
                            >
                                Paid
                            </Typography>
                        </Stack>
                    </Grid>
                    <Grid container display = "flex" direction = "row" justifyContent = "space-between"
                          alignItems = "center">
                        <Grid item style = {{ marginBottom: "1rem" }}>
                            <Typography variant = "subtitle2" className = "shipping-stripe">
                                Shipping Address
                            </Typography>
                            <Typography variant = "subtitle2">
                                <span style = {{ fontWeight: "bold" }}>Name: </span> {client?.clientName}
                            </Typography>
                            <Typography variant = "subtitle2">
                                <span style = {{ fontWeight: "bold" }}>Company Name:</span> {client?.clientCompanyName}
                            </Typography>
                            <Typography variant = "subtitle2">
                                <span style = {{ fontWeight: "bold" }}>Address:</span> {client?.clientAddress}
                            </Typography>
                            <Typography variant = "subtitle2">
                                <span style = {{ fontWeight: "bold" }}>Phone:</span> {client?.clientContactNumber}
                            </Typography>

                        </Grid>

                        <Grid item style = {{ marginBottom: "1rem" }}>
                            <Typography variant = "subtitle2" className = "order-stripe">
                                Order Details
                            </Typography>
                            <Grid display = "flex" flexDirection = "column" alignItems = "flex-end"
                                  justifyContent = "flex-end">
                                <Typography variant = "subtitle2">
                                    <span style = {{ fontWeight: "bold" }}>Order Date: </span> {slicedDate}
                                </Typography>
                                <Typography variant = "subtitle2">
                                    <span style = {{ fontWeight: "bold" }}>Order #: </span> {_id}
                                </Typography>
                                <Typography variant = "subtitle2">
                                    <span style = {{ fontWeight: "bold" }}>Customer ID: </span> {client?._id}
                                </Typography>
                                <Typography variant = "subtitle2">
                                    <span style = {{ fontWeight: "bold" }}>Delivery Method: </span> UPS
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>

                    <TableContainer>
                        {!report ? (
                            <Loading height = {'50vh'}></Loading>
                        ) : (
                            <Table responsive = "sm" aria-label = "customized table">
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell>#</StyledTableCell>
                                        <StyledTableCell align = "left"> Description</StyledTableCell>
                                        <StyledTableCell align = "center">Ordered</StyledTableCell>
                                        <StyledTableCell align = "center">Delivered</StyledTableCell>
                                        <StyledTableCell align = "center">Unit Price</StyledTableCell>
                                        <StyledTableCell align = "right">Total</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <StyledTableRow>
                                        <StyledTableCell sx = {{ fontWeight: 'bold' }}
                                                         align = "left">1.</StyledTableCell>
                                        <StyledTableCell align = "left">{product?.name}</StyledTableCell>
                                        <StyledTableCell align = "center">{deliveryAmount}</StyledTableCell>
                                        <StyledTableCell align = "center">{deliveryAmount}</StyledTableCell>
                                        <StyledTableCell align = "center">${sellingPriceWithComma}</StyledTableCell>
                                        <StyledTableCell align = "right">${totalPriceWithComma}</StyledTableCell>
                                    </StyledTableRow>
                                </TableBody>
                            </Table>
                        )}
                    </TableContainer>

                    <Grid container display = "flex" direction = "row" justifyContent = "space-between"
                          alignItems = "center">
                        <Grid item>
                            <Typography>
                                Thank You for your business!
                            </Typography>
                        </Grid>
                        <Grid item display = "flex" flexDirection = "column" alignItems = "flex-end"
                              justifyContent = "flex-end" marginTop = "1rem">
                            <Typography variant = "subtitle2">
                                <span style = {{ fontWeight: "bold" }}>SUBTOTAL: </span> ${totalPriceWithComma}
                            </Typography>
                            <Typography variant = "subtitle2">
                                <span style = {{ fontWeight: "bold" }}>DISCOUNT: </span>{discount}%
                            </Typography>
                            <Typography variant = "subtitle2">
                            <span
                                style = {{ fontWeight: "bold" }}>SUBTOTAL LESS DISCOUNT: </span> ${subTotalLessDiscountWithComma}
                            </Typography>
                            <Typography variant = "subtitle2">
                                <span style = {{ fontWeight: "bold" }}>TAX RATE: </span> {taxRate}%
                            </Typography>
                            <Typography variant = "subtitle2">
                                <span style = {{ fontWeight: "bold" }}>TOTAL TAX: </span> ${totalTaxWithComma}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid display = "flex" justifyContent = "flex-end" sx = {{
                        borderTop: "1px solid black",
                        borderBottom: "1px solid black",
                        width: "26%",
                        marginLeft: "auto"
                    }}>
                        <Typography variant = "h6"><span
                            style = {{ fontWeight: "bold" }}>Total:</span> ${totalFinalPriceWithComma}</Typography>
                    </Grid>
                    <Grid>
                        <Typography sx = {{ fontWeight: "bold" }}>Terms & Instructions</Typography>
                        <Typography>Please pay within 20days.</Typography>
                        <Typography>Return defective products within 30days period.</Typography>
                    </Grid>
                </Box>
            </Box>
        </Grid>
    );
};

export default DeliveryReport;
