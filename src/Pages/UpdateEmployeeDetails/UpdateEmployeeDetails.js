import "./UpdateEmployeeDetails.css";
import Header from "../Shared/Header/Header";
import PageTitle from "../Shared/PageTitle/PageTitle";
import { Button, Container, Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import toast from "react-hot-toast";
import useItems from "../../hooks/useItems";
import { useState } from "react";


const UpdateEmployeeDetails = () => {
    const { employeeId } = useParams();
    const { register, handleSubmit } = useForm();
    const navigate = useNavigate();
    const [maritalStatus, setMaritalStatus] = useState("");
    const baseUrl = process.env.REACT_APP_BASE_URL;

    //Get Employee data using useItem Hook
    const [{
        first_name,
        last_name,
        position,
        email,
        gender,
        image,
        address,
        mobile_number,
        emergency_contact,
        birth_date,
        joining_date,
        social_security,
        marital_status
    }] = useItems(`${baseUrl}/employee/${employeeId}`);

    const onSubmit = data => {
        console.log({ data })
        const url = `${baseUrl}/employee/${employeeId}`;
        fetch(url, {
            method: "PUT",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result?.modifiedCount === 1) {
                    toast.success('Employee Information Has Been Updated');
                    navigate("/manage-employee");
                }
            });
    };
    //Marital Status Set
    let married = "";
    if (marital_status?.toString() === "married") {
        married = ["Married", "Single"];
    }else if (marital_status?.toLowerCase() === "single") {
        married = ["Single", "Married"];
    }

    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container w-50 mx-auto">
                <PageTitle title = {"Update Employee"}></PageTitle>

                <Container>
                    <Form className = "add-item-border" onSubmit = {handleSubmit(onSubmit)}>
                        <h4 className = " title-color text-center mb-4"
                            style = {{
                                fontWeight: "bold",
                                marginTop: "1rem"
                            }}>{`Update ` + first_name + ' ' + last_name + 's\' ' + 'Information'}</h4>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text" defaultValue = {first_name}
                                          placeholder = "First Name" {...register("first_name", {
                                required: true, maxLength: 20
                            })} />
                            <Form.Control type = "text" style = {{ marginLeft: "1rem" }} defaultValue = {last_name}
                                          placeholder = "Last Name" {...register("last_name", {
                                required: true, maxLength: 20
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "email" defaultValue = {email}
                                          placeholder = "Email" {...register("email", {
                                required: true, pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                            })} />
                        </Form.Group>


                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicBrand">
                            <Form.Control type = "text" defaultValue = {position}
                                          placeholder = "Position" {...register("position", {
                                required: true
                            })} />


                            <Form.Control style = {{ marginLeft: "1rem" }} disabled type = "text" value = {gender}/>
                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicPrice">
                            <Form.Control defaultValue = {mobile_number}
                                          placeholder = "Phone Number" {...register("mobile_number", {
                                required: true,
                                maxLength: 20
                            })} />

                            <Form.Control style = {{ marginLeft: "1rem" }}
                                          defaultValue = {emergency_contact}
                                          placeholder = "Emergency Number" {...register("emergency_contact", {
                                required: true,
                                maxLength: 20
                            })} />

                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicBrand">
                            <Form.Label>Marital Status</Form.Label>
                            <Form.Select as = "select" defaultValue={maritalStatus}
                                         {...register("marital_status", {
                                             required: true
                                         })}>
                                {
                                    married?.length > 0 && married?.map((marital, index) => <option
                                        key = {index}
                                    >{marital}
                                    </option>)
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "description">
                            <Form.Control placeholder = "Address" as = "textarea" defaultValue = {address}
                                          rows = {3} {...register("address", {
                                required: true
                            })} />
                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicStock">
                            <Form.Control disabled type = "number" value = {social_security}/>
                        </Form.Group>

                        <Form.Group className = " mb-3 col-lg forms-control" controlId = "formBasicStock">
                            <Form.Label>Birth Date</Form.Label>
                            <Form.Control disabled className = " mb-3" type = "text" value = {birth_date} />

                            <Form.Label>Joining Date</Form.Label>
                            <Form.Control disabled type = "text" value = {joining_date} />
                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text" defaultValue = {image}
                                          placeholder = "Image URL" {...register("image", {
                                required: true
                            })} />
                        </Form.Group>

                        <Button className = "add-item-btn" style = {{ width: "60%" }}
                                variant = "primary mx-auto d-block mb-5" type = "submit">
                            Update Information
                        </Button>
                    </Form>
                </Container>
            </div>
        </>
    );
};

export default UpdateEmployeeDetails;
