import "./Faqs.css";
import Faq from "react-faq-component";
import { Box, Grid } from "@mui/material";
import PageTitle from "../Shared/PageTitle/PageTitle";
import Header from "../Shared/Header/Header";
import CssBaseline from "@mui/material/CssBaseline";
import useItems from "../../hooks/useItems";
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const Faqs = () => {
    const baseUrl = process.env.REACT_APP_BASE_URL;

    //Get Clients data using useItem Hook
    const [faqs] = useItems(`${baseUrl}/faqs`);

    const styles = {
        bgColor: '#F2F4F8',
        titleTextColor: "#515151",
        rowTitleColor: "#054c9d",
        rowContentColor: 'black',
        arrowColor: "black",
        rowContentPaddingTop: '10px',
        rowContentPaddingBottom: '10px',
        rowContentPaddingLeft: '30px',
        rowContentPaddingRight: '150px',
        transitionDuration: ".8s",
        timingFunc: "ease"
    };

    const config = {
        animate: true,
        arrowIcon: <ExpandMoreIcon/>,
        openOnload: 0,
        tabFocus: true,
        collapseIcon: <ExpandLessIcon/>,
    };

    return (
        <Grid>
            <PageTitle title = {"Faqs"} />
            <Header />
            <CssBaseline />
            <Box component = "main"
                 sx = {{
                     flexGrow: 1, bgcolor: '#F2F4F8', mt: '100px', maxWidth: 1000, margin: '3rem auto 5rem auto'
                 }}>
                { faqs ? <Faq
                    data = {faqs[0]}
                    styles = {styles}
                    config = {config}
                /> : null
                }
            </Box>
        </Grid>

    );
};

export default Faqs;
