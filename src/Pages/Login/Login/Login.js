import React, { useEffect, useRef } from 'react';
import "./Login.css";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useSendPasswordResetEmail, useSignInWithEmailAndPassword } from "react-firebase-hooks/auth";
import auth from "../../../firebase.init";
import useToken from "../../../hooks/useToken";
import toast from "react-hot-toast";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import { Container, Navbar, Form, Button } from "react-bootstrap";
import logo from "../../../images/banner-img-transparent.png";
import SocialLogin from "../SocialLogin/SocialLogin";
import Header from "../../Shared/Header/Header";
import Footer from "../../Shared/Footer/Footer";
import Loading from "../../Shared/Loading/Loading";

const Login = () => {
    const emailRef = useRef('');
    const passwordRef = useRef('');
    const navigate = useNavigate();
    const location = useLocation();
    let from = location.state?.from?.pathname || "/";

    const [signInWithEmailAndPassword, user, loading, error,] = useSignInWithEmailAndPassword(auth);

    const name = user?.user?.displayName;
    const [sendPasswordResetEmail, sending] = useSendPasswordResetEmail(auth);
    const [token] = useToken(user);

    //Navigates user if successfully logs in
    useEffect(() => { //Used useEffect to eliminate error. When condition occurs or, value changes while rendering, wrap it with useEffect. Also, remember to use useEffect after setting something.
        if (token) {
            navigate(from, { replace: true });
            name && toast.success(`Welcome ${name}!`);
        }
    }, [token]);


    const handleLogin = async event => {
        event.preventDefault();
        const email = emailRef.current.value;
        const password = passwordRef.current.value;
        // console.log(email, password);
        await signInWithEmailAndPassword(email, password);
    };

    const navigateRegister = event => {
        navigate('/registration');
    };

    //Error Handling
    if (error) {
        error?.code === 'auth/user-not-found' &&
        toast.error('Account does not exist');

        error?.code === 'auth/wrong-password' &&
        toast.error('Incorrect Password');
    }

    if (loading) {
        return <Loading />
    }

    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container container-width login-page-color login-border">
                <PageTitle title = {"Login"}></PageTitle>
                <Container className = "logo" fluid mt-5 mb-5>
                    <Navbar.Brand href = "/" as = {Link} to = '/'>
                        <img width = "100%" src = {logo} alt = "" />
                    </Navbar.Brand>
                </Container>
                <h3 className = "title-color  text-center mt-3 " style = {{ fontWeight: "bold" }}>Login With</h3>

                <Form onSubmit = {handleLogin}>
                    <Form.Group className = "mb-3" controlId = "formBasicEmail">
                        <Form.Control ref = {emailRef} type = "email" style = {{ width: "50%", margin: "0 auto" }}
                                      placeholder = "Enter email" required />
                    </Form.Group>

                    <Form.Group className = "mb-3" controlId = "formBasicPassword">
                        <Form.Control ref = {passwordRef} type = "password" style = {{ width: "50%", margin: "0 auto" }}
                                      placeholder = "Password" required />
                    </Form.Group>

                    <Link to={"/forgot-password"} style={{textDecoration:"none"}}>
                        <p className = "text-center p-color" style = {{ marginTop: "-8px" }}>Forgot Password? <button
                            className = " btn btn-link text-primary pe-auto text-decoration-none"
                        >Reset Password</button></p>
                    </Link>

                    <Button className = "login-btn" variant = "primary w-50 mx-auto d-block m-2" type = "submit">
                        Login
                    </Button>
                </Form>
                <p className = "text-center p-color mt-4">Don't have an account? <Link to = "/registration"
                                                                                       className = "text-primary pe-auto text-decoration-none"
                                                                                       onClick = {navigateRegister}>Register</Link>
                </p>
                <SocialLogin></SocialLogin>

            </div>
            <Footer />
        </>
    );
};

export default Login;