import React, { useEffect, useState } from 'react';
import "./Registration.css";
import logo from "../../../images/banner-img-transparent.png";
import { Button, Container, Form, Navbar } from "react-bootstrap";
import { useCreateUserWithEmailAndPassword, useUpdateProfile } from "react-firebase-hooks/auth";
import auth from "../../../firebase.init";
import useToken from "../../../hooks/useToken";
import { Link, useNavigate } from "react-router-dom";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import SocialLogin from "../SocialLogin/SocialLogin";
import Header from "../../Shared/Header/Header";
import Footer from "../../Shared/Footer/Footer";
import toast from "react-hot-toast";
import Loading from "../../Shared/Loading/Loading";


const Registration = () => {
    const [agree, setAgree] = useState(false);

    const [
        createUserWithEmailAndPassword,
        user,
        loading,
        error,
    ] = useCreateUserWithEmailAndPassword(auth, { sendEmailVerification: true });

    const [updateProfile] = useUpdateProfile(auth);
    const [token] = useToken(user);
    const navigate = useNavigate();

    const navigatelogin = event => {
        navigate('/login');
    };

    useEffect(() => {
        if (token) {
            navigate('/');
        }
    }, [token]);


    if (error) {
        error?.code === 'auth/email-already-in-use' &&
        toast.error('Email Already Exists');
    }


    const handleRegistration = async (event) => {
        event.preventDefault();
        const name = event.target.name.value;
        const email = event.target.email.value;
        const password = event.target.password.value;
        if (password.length < 6) {
            toast.error('Password must be at least 6 characters long');
        } else {
            await createUserWithEmailAndPassword(email, password);
            await updateProfile({ displayName: name });
        }
        // console.log(name, email, password);
        // console.log('Updated Profile');
    };

    if (loading) {
        return <Loading/>
    }

    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container registration-border registration-page-color">
                <PageTitle title = {"Registration"}></PageTitle>
                <Container className = "logo mt-2">
                    <Navbar.Brand href = "/" as = {Link} to = '/'>
                        <img width = "100%" src = {logo} alt = "" />
                    </Navbar.Brand>
                </Container>
                <h3 className = "title-color  text-center mb-3" style = {{ fontWeight: "bold" }}>Register Yourself</h3>

                <Form className = "text-center" onSubmit = {handleRegistration}>
                    <Form.Group className = "mb-3" controlId = "formBasicName">
                        <Form.Control type = "text" name = "name" style = {{ width: "50%", margin: "0 auto" }}
                                      placeholder = "Full Name" required />
                    </Form.Group>

                    <Form.Group className = "mb-3" controlId = "formBasicEmail">
                        <Form.Control type = "email" name = "email" style = {{ width: "50%", margin: "0 auto" }}
                                      placeholder = "Enter email" required />
                    </Form.Group>

                    <Form.Group className = "mb-3" controlId = "formBasicPassword">
                        <Form.Control type = "password" name = "password" style = {{ width: "50%", margin: "0 auto" }}
                                      placeholder = "Password" required />
                    </Form.Group>

                    <input onClick = {() => setAgree(!agree)} type = "checkbox" name = "terms" id = "terms" />
                    <label className = {agree ? 'ps-2 text-primary' : 'ps-2 text-danger'} htmlFor = "terms">Accept
                        Volunteer
                        Network Terms and Conditions</label>

                    <Button className = "registration-btn" disabled = {!agree}
                            variant = "primary w-50 mx-auto d-block m-2"
                            type = "submit">
                        Register
                    </Button>
                </Form>
                <SocialLogin></SocialLogin>

                <p className = "p-color text-center">Already have an account? <button
                    className = "btn btn-link text-primary pe-auto p-color text-decoration-none"
                    onClick = {navigatelogin}>Login</button></p>
            </div>
            <Footer />
        </>
    );
};

export default Registration;