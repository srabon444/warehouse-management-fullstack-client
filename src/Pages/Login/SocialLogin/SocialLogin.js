import React, { useEffect, useState } from 'react';
import "./SocialLogin.css"
import { useSignInWithGoogle } from "react-firebase-hooks/auth";
import auth from "../../../firebase.init";
import useToken from "../../../hooks/useToken";
import { useLocation, useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";
import toast from "react-hot-toast";

const SocialLogin = () => {
    const [signInWithGoogle, user, loading, error] = useSignInWithGoogle(auth);
    const [token] = useToken(user);
    const navigate = useNavigate();
    const location = useLocation();
    let from = location.state?.from?.pathname || "/";
    const [errorMessage, setErrorMessage] = useState('');
    const name = user?.user?.displayName;

    useEffect(() => {
        if (error) {
            setErrorMessage(error.message);
        }
    }, [error]);

    // console.log({ token });

    useEffect(() => {
        if (token) {
            // console.log({ from });
            navigate(from, { replace: true });
            name && toast.success(`Welcome ${name}!`);
        }
    }, [token]);

    // console.log("From Social Login", user);
    return (
        <div style={{textAlign:"center"}}>
            <div className="divider">OR</div>
            {errorMessage && <p className = "text-danger">Error: {error.message}</p>}
            <Button onClick = {() => {
                signInWithGoogle();
                // console.log(user);
            }} className = "google-button"
                    variant = "w-50 mx-auto d-block m-2" type = "submit">
                Continue with Google
            </Button>
        </div>
    );
};

export default SocialLogin;

