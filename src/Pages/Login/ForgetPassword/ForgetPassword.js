import React, { useRef } from "react";
import "./ForgetPassword.css";
import { Link, useNavigate } from "react-router-dom";
import { useSendPasswordResetEmail } from "react-firebase-hooks/auth";
import auth from "../../../firebase.init";
import toast from "react-hot-toast";
import Loading from "../../Shared/Loading/Loading";
import Header from "../../Shared/Header/Header";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import { Button, Container, Form, Navbar } from "react-bootstrap";
import logo from "../../../images/banner-img-transparent.png";
import Footer from "../../Shared/Footer/Footer";

const ForgetPassword = () => {
    const [sendPasswordResetEmail, loading] = useSendPasswordResetEmail(auth)
    const navigate = useNavigate();
    const emailRef = useRef('');

    //Reset Password
    const handleResetPassword = async event => {
        const email = emailRef.current.value;
        if (email) {
            await sendPasswordResetEmail(email);
            toast.success('Reset Email Sent');
        } else {
            toast.error("Please Enter Your Email Address");
        }
    };

    const navigateRegister = event => {
        navigate('/registration');
    };

    if (loading) {
        return <Loading />
    }

    return (
        <>
            <div style = {{ marginBottom: "2rem" }}>
                <Header />
            </div>
            <div className = "container container-width login-page-color reset-password-border">
                <PageTitle title = {"Reset Password"}></PageTitle>
                <Container className = "logo" fluid mt-5 mb-5>
                    <Navbar.Brand href = "/" as = {Link} to = '/'>
                        <img width = "100%" src = {logo} alt = "" />
                    </Navbar.Brand>
                </Container>
                <h3 className = "title-color  text-center mt-3 mb-4 " style = {{ fontWeight: "bold" }}>Forgot
                    Password?</h3>

                <Form onSubmit = {handleResetPassword}>
                    <Form.Group className = "mb-3" controlId = "formBasicEmail">
                        <Form.Control ref = {emailRef} type = "email" style = {{ width: "50%", margin: "0 auto" }}
                                      placeholder = "Enter email" required />
                    </Form.Group>

                    <Button className = "reset-btn" variant = "primary w-50 mx-auto d-block m-2" type = "submit">
                        Reset Password
                    </Button>
                </Form>
                <p className = "text-center p-color mt-4">Don't have an account? <Link to = "/registration"
                                                                                       className = "text-primary pe-auto text-decoration-none"
                                                                                       onClick = {navigateRegister}>Register</Link>
                </p>

            </div>
            <Footer />
        </>
    );
};

export default ForgetPassword;
