import React, { useEffect, useState } from 'react';
import "./ManageSuppliers.css";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import {
    Box, Button,
    Grid,
    Table, TableBody,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import Header from "../../Shared/Header/Header";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Loading from "../../Shared/Loading/Loading";
import AddCircleRoundedIcon from "@mui/icons-material/AddCircleRounded";
import Footer from "../../Shared/Footer/Footer";
import ManageSuppliersItem from "../ManageSuppliersItem/ManageSuppliersItem";
import StyledTableCell from "../../../StyledComponents/StyledTableCell";
import StyledTableRow from "../../../StyledComponents/StyledTableRow";
import 'react-confirm-alert/src/react-confirm-alert.css';

const ManageSuppliers = () => {
    const [suppliers, setSuppliers] = useState([]);
    const baseUrl = process.env.REACT_APP_BASE_URL;

    // Get Inventory Data
    const suppliersUrl = `${baseUrl}/suppliers`;

    useEffect(() => {
        axios.get(suppliersUrl)
            .then((res) => {
                setSuppliers(res.data)
            });
    }, []);

    // Delete a Supplier from Database
    const handleDeleteSupplier = (id) => {
        confirmAlert({
            title: 'Confirm to delete supplier',
            message: 'Are you sure you want to delete supplier?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        axios.delete(`${baseUrl}/supplier/${id}`)
                            .then((res) => {
                                const remainingSuppliers = suppliers.filter(
                                    (supplier) => supplier._id !== id
                                );
                                setSuppliers(remainingSuppliers);
                                toast.success('Supplier Deleted Successfully!', {
                                    position: "top-right"
                                });
                                // window.location.reload(false);
                            })
                            .catch((err) => {
                                toast.error(err);
                            })
                    },
                },
                {
                    label: 'No',
                    onClick: () => '',
                },
            ],
        });
    };

    const navigate = useNavigate();

    return (
        <Grid>
            <PageTitle title = {"Manage Supplier"} />
            <Header />
            <CssBaseline />
            <Box component = "main"
                 sx = {{
                     flexGrow: 1, bgcolor: '#F2F4F8', mt: '100px', maxWidth: 1000, margin: '0 auto'
                 }}>

                {/*Table Begin*/}
                <Grid className = 'table-bar'>
                    <Typography variant = "h1"
                                fontSize = {30}
                                fontWeight = {"bold"}
                                color = "#2c425d"
                                sx = {{ padding: "1rem 0" }}
                    >
                        Manage All Suppliers
                    </Typography>

                </Grid>

                <TableContainer>
                    {!suppliers.length ? (
                        <Loading height = {'50vh'}></Loading>
                    ) : (
                        <Table responsive="sm" aria-label = "customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell>#</StyledTableCell>
                                    <StyledTableCell>Suppliers' Name</StyledTableCell>
                                    <StyledTableCell>Email</StyledTableCell>
                                    <StyledTableCell align = "left">Address</StyledTableCell>
                                    <StyledTableCell align = "left">Contact Number</StyledTableCell>
                                    <StyledTableCell align = "right">Manage</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    suppliers?.length > 0 && suppliers?.map((supplier, index) => (
                                        <StyledTableRow hover
                                                        key = {supplier._id}
                                        >
                                            <ManageSuppliersItem
                                                index = {index}
                                                supplier = {supplier}
                                                deleteSupplier={handleDeleteSupplier}
                                            />
                                        </StyledTableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    )}
                </TableContainer>

                {/*Add Supplier Button*/}
                <Grid display = "flex" justifyContent = "space-between" mt = '25px' mb = '5rem'>
                    <Button sx = {{ padding: ".5rem 1.5rem" }} className = "add-suppliers-btn"
                            onClick = {() => {
                                navigate('/add-supplier')
                            }}
                    >
                        <AddCircleRoundedIcon sx = {{ marginRight: '.8rem' }} />
                        Add Supplier
                    </Button>
                </Grid>

            </Box>
            <Footer />
        </Grid>
    );
};

export default ManageSuppliers;
