import React from 'react';
import { useNavigate } from "react-router-dom";
import EditRoundedIcon from "@mui/icons-material/EditRounded";
import DeleteForeverRoundedIcon from "@mui/icons-material/DeleteForeverRounded";
import StyledTableCell from "../../../StyledComponents/StyledTableCell";

const ManageSuppliersItem = ({index, supplier, deleteSupplier}) => {
    const {_id, name, email, address, contact} = supplier;
    const navigate = useNavigate();
    return (
        <>
            <StyledTableCell sx = {{ fontWeight: 'bold' }} align = "left">
                {index + 1}.</StyledTableCell>
            <StyledTableCell  align = "left" sx = {{ fontWeight: 'bold' }} scope = "row">{name}</StyledTableCell>
            <StyledTableCell align = "left">{email}</StyledTableCell>
            <StyledTableCell align = "left">{address}</StyledTableCell>
            <StyledTableCell align = "left">{contact}</StyledTableCell>

            <StyledTableCell align = "right">
                <EditRoundedIcon
                    sx = {{ color: '#1976D2', cursor: 'pointer' }}
                    onClick = {() => navigate(`/supplier/${_id}`)}
                />{"  "}

                <DeleteForeverRoundedIcon
                    sx = {{ color: 'red', cursor: 'pointer' }}
                    onClick = {() => deleteSupplier(_id)}
                />
            </StyledTableCell>
        </>
    );
};

export default ManageSuppliersItem;
