import React from 'react';
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import Header from "../../Shared/Header/Header";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import { Button, Container, Form } from "react-bootstrap";
import "./AddSuplliers.css";

const AddSuppliers = () => {
    const { register, handleSubmit, reset } = useForm();
    const baseUrl = process.env.REACT_APP_BASE_URL;

    const onSubmit = data => {
        const url = `${baseUrl}/supplier`;
        fetch(url, {
            method: "POST",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result.insertedId) {
                    toast.success('New supplier has been added');
                    reset();
                }
            });
    };
    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container w-50 mx-auto">
                <PageTitle title = {"Add Supplier"}></PageTitle>

                <Container>
                    <Form className = "add-supplier-border" onSubmit = {handleSubmit(onSubmit)}>
                        <h4 className = " title-color text-center mb-4"
                            style = {{ fontWeight: "bold", marginTop: "1rem" }}>Add New Supplier</h4>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text"
                                          placeholder = "Suppliers' Name" {...register("name", {
                                required: true, maxLength: 20
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "email"
                                          placeholder = "Email" {...register("email", {
                                required: true, pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicPrice">
                            <Form.Control type = "number"
                                          placeholder = "Phone Number" {...register("contact", {
                                required: true,
                                minLength:8,
                                maxLength: 12
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "description">
                            <Form.Control placeholder = "Address" as = "textarea"
                                          rows = {3} {...register("address", {
                                required: true
                            })} />
                        </Form.Group>

                        <Button className = "add-supplier-btn" style = {{ width: "60%" }}
                                variant = "primary mx-auto d-block mb-5" type = "submit">
                            Add Supplier
                        </Button>
                    </Form>
                </Container>
            </div>
        </>
    );
};

export default AddSuppliers;
