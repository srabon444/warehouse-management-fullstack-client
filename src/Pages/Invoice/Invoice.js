import React, {useState} from 'react';
import "./Invoice.css";
import {Box, Grid, Stack, TextField, Tooltip} from "@mui/material";
import PageTitle from "../Shared/PageTitle/PageTitle";
import Header from "../Shared/Header/Header";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import {useNavigate} from "react-router-dom";
import toast from "react-hot-toast";

const Invoice = () => {
  const [deliveryID, setDeliveryID] = useState("");
  const navigate = useNavigate();

  const handleSearch = () => {
    if (deliveryID === "") {
      toast.error("Please Enter an Order ID")
    } else if (deliveryID.length < 24 || deliveryID.length > 24) {
      toast.error("Please Enter Correct Order ID")
    } else if (deliveryID.length === 24) {
      navigate(`/delivery-details/${deliveryID}`);
    }
  }

  return (
      <Grid>
        <PageTitle title={"Search Invoice"}/>
        <Header/>
        <CssBaseline/>

        <Box component="main"
             sx={{
               flexGrow: 1,
               bgcolor: '#F2F4F8',
               mt: '4rem',
               maxWidth: 900,
               margin: '2rem auto',
             }}>
          <Grid className='invoice-table-bar' alignItems='center' sx={{border: '2px solid #d3d3d3',}}>
            <Typography variant="h1"
                        fontSize={30}
                        fontWeight={"bold"}
                        color="#2c425d"
                        sx={{padding: "1rem 0"}}
            >
              Search Invoice
            </Typography>
            <Stack direction="row" alignItems="center" justifyContent="center" gap={1}>
              <Tooltip placement="top-start" title={"Enter Order ID"}>
                <TextField size="small" id="outlined-basic" label="Order ID" variant="outlined"
                           onChange={(e) => setDeliveryID(e.target.value)}/>
              </Tooltip>
              <Tooltip placement="top-start" title={"Search"}>
                <SearchRoundedIcon className="invoice-search-icon" sx={{fontSize: 40}}
                                   onClick={() => handleSearch()}/>
              </Tooltip>
            </Stack>
          </Grid>
          <Tooltip placement="bottom-start" title={"Use This ID For Testing"}>
            <Typography fontWeight='bold'><span
                style={{borderBottom: '1px solid black'}}>Example Order ID:</span> 63a190430b5328aebfa8b090</Typography>
          </Tooltip>
        </Box>
      </Grid>
  );
};

export default Invoice;
