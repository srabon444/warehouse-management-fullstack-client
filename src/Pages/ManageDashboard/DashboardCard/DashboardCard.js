import { Card, CardContent, Stack } from "@mui/material";
import Typography from "@mui/material/Typography";
import VerifiedRoundedIcon from '@mui/icons-material/VerifiedRounded';
import CategoryRoundedIcon from '@mui/icons-material/CategoryRounded';
import StoreRoundedIcon from '@mui/icons-material/StoreRounded';
import PeopleAltRoundedIcon from '@mui/icons-material/PeopleAltRounded';
import ReceiptRoundedIcon from '@mui/icons-material/ReceiptRounded';
import ShoppingBasketRoundedIcon from '@mui/icons-material/ShoppingBasketRounded';
import PointOfSaleRoundedIcon from '@mui/icons-material/PointOfSaleRounded';
import MonetizationOnRoundedIcon from '@mui/icons-material/MonetizationOnRounded';
import "./DashboardCard.css";

const DashboardCard = ({ cardType, value }) => {

    const valueWithComma = value
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    // Card Value with Dollar Sign or, not
    const cardValueWithSign = () => {
        let valueWithSign;
        if (cardType === "totalPurchase" || cardType === "totalSales" || cardType === "totalProfit") {
            valueWithSign = "$" + valueWithComma.toString();
        } else {
            valueWithSign = valueWithComma.toString();
        }
        return valueWithSign;
    }

    // Card Value Color
    const valueColor = () => {
        let valueColor;
        if (cardType === "totalStock") {
            valueColor = "#55A292"
        }
        if (cardType === "totalCategory") {
            valueColor = "#8F7568"
        }
        if (cardType === "totalSupplier") {
            valueColor = "#365DA3"
        }
        if (cardType === "totalClients") {
            valueColor = "#E68871"
        }
        if (cardType === "countSales") {
            valueColor = "#E3656F"
        }
        if (cardType === "totalPurchase") {
            valueColor = "#919EAF"
        }
        if (cardType === "totalSales") {
            valueColor = "#824985"
        }
        if (cardType === "totalProfit") {
            valueColor = "#CD4F56"
        }
        return valueColor;
    };

    // Card Type Full Name
    const cardTypeFullName = () => {
        let cardFullName;
        if (cardType === "totalStock") {
            cardFullName = "Total Stock"
        }
        if (cardType === "totalCategory") {
            cardFullName = "Category"
        }
        if (cardType === "totalSupplier") {
            cardFullName = "Supplier"
        }
        if (cardType === "totalClients") {
            cardFullName = "Clients"
        }
        if (cardType === "countSales") {
            cardFullName = "Number of Sales"
        }
        if (cardType === "totalPurchase") {
            cardFullName = "Total Purchase Amount"
        }
        if (cardType === "totalSales") {
            cardFullName = "Total Sales Amount"
        }
        if (cardType === "totalProfit") {
            cardFullName = "Total Profit"
        }

        return cardFullName;
    }

    // Card Icon Function
    const cardIconFunction = () => {
        let cardIcon;
        if (cardType === "totalStock") {
            cardIcon = <VerifiedRoundedIcon/>
        }
        if (cardType === "totalCategory") {
            cardIcon = <CategoryRoundedIcon/>
        }
        if (cardType === "totalSupplier") {
            cardIcon = <StoreRoundedIcon/>
        }
        if (cardType === "totalClients") {
            cardIcon = <PeopleAltRoundedIcon/>
        }
        if (cardType === "countSales") {
            cardIcon = <ReceiptRoundedIcon/>
        }
        if (cardType === "totalPurchase") {
            cardIcon = <ShoppingBasketRoundedIcon/>
        }
        if (cardType === "totalSales") {
            cardIcon = <PointOfSaleRoundedIcon/>
        }
        if (cardType === "totalProfit") {
            cardIcon = <MonetizationOnRoundedIcon/>
        }
        return cardIcon;
    }

    return (
        <>
            <Card sx = {{ minWidth: 280, maxWidth: 280, minHeight: 140, maxHeight: 140, borderRadius: '15px' }}
                  className = "dashboard-card">
                <CardContent>
                    <Stack display = "flex" direction = "row" alignItems = "center" justifyContent = "space-between" paddingX = ".5rem">
                        <Typography fontWeight = 'bold' variant = "h4" color={valueColor()}>{cardValueWithSign()}</Typography>
                        <Typography color="#9ba3ac">
                            {cardIconFunction()}
                        </Typography>
                    </Stack>
                    <Typography paddingLeft = ".5rem" paddingTop = "2rem" variant = "subtitle1" color="#868585" fontWeight = "bold">{cardTypeFullName()}</Typography>
                </CardContent>
            </Card>
        </>
    );
};

export default DashboardCard;
