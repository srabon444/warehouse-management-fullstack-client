import React, { useEffect, useState } from 'react';
import axios from "axios";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import CssBaseline from "@mui/material/CssBaseline";
import Header from "../../Shared/Header/Header";
import { Box, Grid, Table, TableBody, TableContainer, TableHead, TableRow } from "@mui/material";
import Typography from "@mui/material/Typography";
import Loading from "../../Shared/Loading/Loading";
import StyledTableCell from "../../../StyledComponents/StyledTableCell";
import StyledTableRow from "../../../StyledComponents/StyledTableRow";
import ManageDashboardItem from "../ManageDashboardItem/ManageDashboardItem";
import "./ManageDashboard.css";
import DashboardCard from "../DashboardCard/DashboardCard";
import { useAuthState } from "react-firebase-hooks/auth";
import auth from "../../../firebase.init";
import Footer from "../../Shared/Footer/Footer";

const ManageDashboard = () => {
    const [lowStockProducts, setLowStockProducts] = useState([]);
    const [dashboardData, setDashboardData] = useState(null);
    const baseUrl = process.env.REACT_APP_BASE_URL;
    const [user] = useAuthState(auth);


    // Get Inventory Data
    const lowStockProductsUrl = `${baseUrl}/low-stock-products`;
    useEffect(() => {
        axios.get(lowStockProductsUrl)
            .then((res) => {
                setLowStockProducts(res.data)
            });
    }, []);

    // Get Dashboard Data
    const dashboardDataURL = `${baseUrl}/dashboard-data`;
    useEffect(() => {
        console.log("hit", dashboardDataURL);
        axios.get(dashboardDataURL)
            .then((res) => {
                setDashboardData(res?.data);
                console.log(res.data)
            })
            .catch((err) => {
                setDashboardData(null);
                console.log({ err })
            })
    }, []);

    console.log({ dashboardData })

    const getWelcomeMessage = () => {
        let today = new Date()
        let curHr = today.getHours()
        let welcomeMessage;
        if (curHr < 12) {
            welcomeMessage = 'Good Morning, ';
        } else if (curHr < 18) {
            welcomeMessage = 'Good Afternoon, ';
        } else {
            welcomeMessage = 'Good Evening, ';
        }
        return welcomeMessage;
    }

    return (
        <Grid>
            <PageTitle title = {"Dashboard"} />
            <Header />
            <CssBaseline />

            <Box component = "main"
                 sx = {{
                     flexGrow: 1, bgcolor: '#F2F4F8', mt: '100px', maxWidth: 1200, margin: '0 auto'
                 }}>

                <Typography margin = "2rem 0 1rem 0" variant = "h3" fontWeight="bold" color="#5C5D66">{getWelcomeMessage()}{user?.displayName}{"!"}</Typography>
                <Typography variant="body1" fontSize="large" color="#2C425D">Welcome to your shipido Inventory Management System.</Typography>

                {/*Table Begin*/}
                <Grid className = 'dashboard-table-bar'>
                    <Typography variant = "h1"
                                fontSize = {30}
                                fontWeight = {"bold"}
                                color = "#2c425d"
                                sx = {{ padding: "1rem 0" }}
                    >Dashboard</Typography>
                </Grid>

                {/*Dashboard*/}
                <Grid className = "dashboard-card-area">
                    {
                        dashboardData !== null &&
                        Object.keys(dashboardData?.dashboard).map((data, index) => (
                            <DashboardCard
                                key = {index}
                                value = {dashboardData.dashboard[data]}
                                cardType = {data}
                            />
                        ))
                    }
                </Grid>

                {/*Low Stock Inventory table*/}
                <Typography variant = "h5" fontWeight = "bold" color = "#2c425d" margin = "2.5rem 0 1.2rem 0">Low Stock
                    Inventory</Typography>
                <TableContainer >
                    {!lowStockProducts.length ? (
                        <Loading height = {'50vh'}></Loading>
                    ) : (
                        <Table responsive = "sm" aria-label = "customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell>#</StyledTableCell>
                                    <StyledTableCell>Name</StyledTableCell>
                                    <StyledTableCell align = "left">Image</StyledTableCell>
                                    <StyledTableCell align = "left">Buying Price</StyledTableCell>
                                    <StyledTableCell align = "left">Selling Price</StyledTableCell>
                                    <StyledTableCell align = "center">Stock</StyledTableCell>
                                    <StyledTableCell align = "center">Action</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    lowStockProducts?.length > 0 && lowStockProducts?.map((product, index) => (
                                        <StyledTableRow hover
                                                        key = {product._id}
                                        >
                                            <ManageDashboardItem
                                                index = {index}
                                                product = {product}
                                            />
                                        </StyledTableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    )}
                </TableContainer>
            </Box>
            <Footer/>
        </Grid>
    );
};

export default ManageDashboard;
