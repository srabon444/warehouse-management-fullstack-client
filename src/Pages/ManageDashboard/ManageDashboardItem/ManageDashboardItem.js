import React from 'react';
import StyledTableCell from "../../../StyledComponents/StyledTableCell";
import { Chip, Tooltip } from "@mui/material";
import SyncRoundedIcon from "@mui/icons-material/SyncRounded";
import { useNavigate } from "react-router-dom";


const ManageDashboardItem = ({index, product}) => {
    const {_id, name, image, sellingPrice, buyingPrice, stock} = product;
    const navigate = useNavigate();

    // Short Name
    const shortName = name?.slice(0, 50);

    // Comma added for price
    const buyingPriceWithComma = buyingPrice
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    const sellingPriceWithComma = sellingPrice
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return (
        <>
            <StyledTableCell sx = {{ fontWeight: 'bold' }} align = "left">{index + 1}.</StyledTableCell>
            <Tooltip title = {name} arrow>
                <StyledTableCell scope = "row">{shortName}...</StyledTableCell>
            </Tooltip>
            <StyledTableCell width = "2rem" align = "right">
                <img width = "100%" src = {image} alt = "" />
            </StyledTableCell>
            <StyledTableCell align = "left">${buyingPriceWithComma}</StyledTableCell>
            <StyledTableCell align = "left">${sellingPriceWithComma}</StyledTableCell>
            <StyledTableCell align = "center">
                {
                    stock ? stock :
                        <Chip
                            sx = {{ fontWeight: 'bold' }}
                            label = "Sold Out"
                            variant = "outlined"
                            color = 'error'
                        />
                }
            </StyledTableCell>
            <StyledTableCell align = "center">
                <Tooltip title = "Update Stock">
                    <SyncRoundedIcon color = "success"
                                     sx = {{ cursor: 'pointer' }}
                                     onClick = {() => navigate(`/inventory/stock-update/${_id}`)}
                    />
                </Tooltip>{" "}
            </StyledTableCell>
        </>
    );
};

export default ManageDashboardItem;
