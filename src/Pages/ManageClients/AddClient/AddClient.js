import React from 'react';
import Header from "../../Shared/Header/Header";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import { Button, Container, Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";

const AddClient = () => {
    const { register, handleSubmit, reset } = useForm();

    const baseUrl = process.env.REACT_APP_BASE_URL;

    const onSubmit = data => {
        const url = `${baseUrl}/client`;
        fetch(url, {
            method: "POST",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result?.insertedId) {
                    toast.success('New Client Has Been Added');
                    reset();
                }
            });
    };
    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container w-50 mx-auto">
                <PageTitle title = {"Add Client"}></PageTitle>

                <Container>
                    <Form className = "add-employee-border" onSubmit = {handleSubmit(onSubmit)}>
                        <h4 className = " title-color text-center mb-4"
                            style = {{ fontWeight: "bold", marginTop: "1rem" }}>Add New Client</h4>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text"
                                          placeholder = "Name" {...register("clientName", {
                                required: true, maxLength: 20
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "email"
                                          placeholder = "Email" {...register("clientEmail", {
                                required: true, pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                            })} />
                        </Form.Group>


                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicBrand">
                            <Form.Control type = "text"
                                          placeholder = "Company Name" {...register("clientCompanyName", {
                                required: true
                            })} />
                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicPrice">
                            <Form.Control type = "number"
                                          placeholder = "Contact Number" {...register("clientContactNumber", {
                                required: true,
                                minLength:8,
                                maxLength: 12
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "description">
                            <Form.Control placeholder = "Address" as = "textarea"
                                          rows = {3} {...register("clientAddress", {
                                required: true
                            })} />
                        </Form.Group>

                        <Button className = "add-btn" style = {{ width: "60%" }}
                                variant = "primary mx-auto d-block mb-5" type = "submit">
                            Add Client
                        </Button>
                    </Form>
                </Container>
            </div>
        </>
    );
};

export default AddClient;
