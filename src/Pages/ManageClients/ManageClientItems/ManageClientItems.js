import React from 'react';
import { useNavigate } from "react-router-dom";
import StyledTableCell from "../../../StyledComponents/StyledTableCell";
import { Tooltip } from "@mui/material";
import EditRoundedIcon from "@mui/icons-material/EditRounded";
import DeleteForeverRoundedIcon from "@mui/icons-material/DeleteForeverRounded";

const ManageClientItems = ({index, client, deleteClient}) => {
    const {_id, clientName, clientEmail, clientCompanyName, clientContactNumber, clientAddress} = client;
    const navigate = useNavigate();

    return (
        <>
            <StyledTableCell sx = {{ fontWeight: 'bold' }} align = "left">{index + 1}.</StyledTableCell>
            <StyledTableCell align = "left">{clientName}</StyledTableCell>
            <StyledTableCell align = "left">{clientEmail}</StyledTableCell>
            <StyledTableCell align = "left">{clientCompanyName}</StyledTableCell>
            <StyledTableCell align = "left">{clientContactNumber}</StyledTableCell>
            <StyledTableCell align = "left">{clientAddress}</StyledTableCell>
            <StyledTableCell align = "right">
                <Tooltip title = "Edit Client Details">
                    <EditRoundedIcon
                        sx = {{ color: '#1976D2', cursor: 'pointer' }}
                        onClick = {() => navigate(`/client/${_id}`)}
                    />
                </Tooltip>{"  "}

                <Tooltip title = "Delete Client">
                    <DeleteForeverRoundedIcon
                        sx = {{ color: 'red', cursor: 'pointer' }}
                        onClick = {() => deleteClient(_id)}
                    />
                </Tooltip>
            </StyledTableCell>
        </>
    );
};

export default ManageClientItems;
