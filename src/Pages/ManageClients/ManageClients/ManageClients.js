import React, { useEffect, useState } from 'react';
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import Header from "../../Shared/Header/Header";
import CssBaseline from "@mui/material/CssBaseline";
import "./ManageClients.css";
import {
    Box, Button,
    Grid,
    Table, TableBody,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import Typography from "@mui/material/Typography";
import Loading from "../../Shared/Loading/Loading";
import StyledTableCell from "../../../StyledComponents/StyledTableCell";
import StyledTableRow from "../../../StyledComponents/StyledTableRow";
import Footer from "../../Shared/Footer/Footer";
import 'react-confirm-alert/src/react-confirm-alert.css';
import ManageClientItems from "../ManageClientItems/ManageClientItems";
import AddCircleRoundedIcon from "@mui/icons-material/AddCircleRounded";

const ManageClients = () => {
    const [clients, setClients] = useState([]);
    const baseUrl = process.env.REACT_APP_BASE_URL;
    const navigate = useNavigate();

    // Get Client Data
    const clientUrl = `${baseUrl}/clients`;

    useEffect(() => {
        axios.get(clientUrl)
            .then((res) => {
                setClients(res.data)
            });
    }, []);

    // Delete a client from Database
    const handleDeleteClient = (id) => {
        confirmAlert({
            title: 'Confirm to delete client',
            message: 'Are you sure you want to delete this client?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        axios.delete(`${baseUrl}/client/${id}`
                        )
                            .then((res) => {
                                const remainingClients = clients.filter(
                                    (client) => client._id !== id
                                );
                                setClients(remainingClients);
                                toast.success('Client Deleted Successfully!', {
                                    position: "top-right"
                                });
                                // window.location.reload(false);
                            })
                            .catch((err) => {
                                toast.error(err);
                            })
                    },
                },
                {
                    label: 'No',
                    onClick: () => '',
                },
            ],
        });
    };


    return (
        <Grid>
            <PageTitle title = {"Manage Clients"} />
            <Header />
            <CssBaseline />
            <Box component = "main"
                 sx = {{
                     flexGrow: 1, bgcolor: '#F2F4F8', mt: '100px', maxWidth: 1100, margin: '0 auto'
                 }}>

                {/*Table Begin*/}
                <Grid className = 'client-table-bar'>
                    <Typography variant = "h1"
                                fontSize = {30}
                                fontWeight = {"bold"}
                                color = "#2c425d"
                                sx = {{ padding: "1rem 0" }}
                    >
                        Manage All Clients
                    </Typography>
                </Grid>

                <TableContainer>
                    {!clients?.length ? (
                        <Loading height = {'50vh'}></Loading>
                    ) : (
                        <Table responsive = "sm" aria-label = "customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell>#</StyledTableCell>
                                    <StyledTableCell>Name</StyledTableCell>
                                    <StyledTableCell align = "left">Email</StyledTableCell>
                                    <StyledTableCell align = "left">Company</StyledTableCell>
                                    <StyledTableCell align = "left">Contact</StyledTableCell>
                                    <StyledTableCell align = "left">Address</StyledTableCell>
                                    <StyledTableCell align = "right">Manage</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    clients?.length > 0 && clients?.map((client, index) => (
                                        <StyledTableRow hover
                                                        key = {client._id}
                                        >
                                            <ManageClientItems
                                                index = {index}
                                                client = {client}
                                                deleteClient = {handleDeleteClient}
                                            />
                                        </StyledTableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    )}
                </TableContainer>

                {/*Add Client Button*/}
                <Grid display = "flex" justifyContent = "space-between" mt = '25px' mb = '5rem'>
                    <Button sx = {{ padding: ".5rem 1.5rem" }} className = "add-client-btn"
                            onClick = {() => {
                                navigate('/add-client')
                            }}
                    >
                        <AddCircleRoundedIcon sx = {{ marginRight: '.8rem' }} />
                        Add Client
                    </Button>
                </Grid>
            </Box>
            <Footer />
        </Grid>
    );
};

export default ManageClients;
