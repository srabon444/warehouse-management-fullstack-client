import React from 'react';
import { useNavigate, useParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import useItems from "../../hooks/useItems";
import toast from "react-hot-toast";
import Header from "../Shared/Header/Header";
import PageTitle from "../Shared/PageTitle/PageTitle";
import { Button, Container, Form } from "react-bootstrap";

const UpdateSupplier = () => {
    const { supplierId } = useParams();
    const { register, handleSubmit } = useForm();
    const navigate = useNavigate();
    const baseUrl = process.env.REACT_APP_BASE_URL;

    //Get Supplier data using useItem Hook
    const [{
        name,
        email,
        address,
        contact
    }] = useItems(`${baseUrl}/supplier/${supplierId}`);

    const onSubmit = data => {
        console.log({ data })
        const url = `${baseUrl}/supplier/${supplierId}`;
        fetch(url, {
            method: "PUT",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result?.modifiedCount === 1) {
                    toast.success('Supplier Information Has Been Updated');
                    navigate("/suppliers");
                    // reset();
                }
                // console.log('Result: ', result);
            });
    };
    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container w-50 mx-auto">
                <PageTitle title = {"Update Supplier"}></PageTitle>

                <Container>
                    <Form className = "add-item-border" onSubmit = {handleSubmit(onSubmit)}>
                        <h4 className = " title-color text-center mb-4"
                            style = {{
                                fontWeight: "bold",
                                marginTop: "1rem"
                            }}>{`Update ` + name + 's\' ' + 'Information'}</h4>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text" defaultValue = {name}
                                          placeholder = "Name" {...register("name", {
                                required: true, maxLength: 20
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "email" defaultValue = {email}
                                          placeholder = "Email" {...register("email", {
                                required: true, pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                            })} />
                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicPrice">
                            <Form.Control type = "number" defaultValue = {contact}
                                          placeholder = "Phone Number" {...register("contact", {
                                required: true,
                                maxLength: 15
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "description">
                            <Form.Control placeholder = "Address" as = "textarea" defaultValue = {address}
                                          rows = {3} {...register("address", {
                                required: true
                            })} />
                        </Form.Group>

                        <Button className = "add-item-btn" style = {{ width: "60%" }}
                                variant = "primary mx-auto d-block mb-5" type = "submit">
                            Update Information
                        </Button>
                    </Form>
                </Container>
            </div>
        </>
    );
};

export default UpdateSupplier;
