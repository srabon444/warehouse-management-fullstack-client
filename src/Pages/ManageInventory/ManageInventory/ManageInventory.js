import React, { useEffect, useState } from 'react';
import PageTitle from "../../Shared/PageTitle/PageTitle";
import CssBaseline from "@mui/material/CssBaseline";
import {
    Box, Button, FormControl, Grid, InputLabel, MenuItem, Pagination, Select,
    Table,
    TableBody,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import Header from "../../Shared/Header/Header";
import axios from "axios";
import { confirmAlert } from 'react-confirm-alert';
import toast from 'react-hot-toast';
import { useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import ManageInventoryItem from "../ManageInventoryItem/ManageInventoryItem";
import "./ManageInventory.css";
import Loading from "../../Shared/Loading/Loading";
import Footer from "../../Shared/Footer/Footer";
import AddCircleRoundedIcon from '@mui/icons-material/AddCircleRounded';
import 'react-confirm-alert/src/react-confirm-alert.css';
import StyledTableCell from "../../../StyledComponents/StyledTableCell";
import StyledTableRow from "../../../StyledComponents/StyledTableRow";

const ManageInventory = () => {
    //Total Page Number
    const [items, setItems] = useState([]);
    const [pages, setPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [filter, setFilter] = useState(10);
    const baseUrl = process.env.REACT_APP_BASE_URL;

    // Get Inventory Data
    const itemsUrl = `${baseUrl}/items?page=${currentPage}&filter=${filter}`;

    useEffect(() => {
        axios.get(itemsUrl)
            .then((res) => {
                setItems(res.data)
            });
    }, [currentPage, filter]);

    // Get Product Count
    const pagesUrl = `${baseUrl}/items-pages`;

    useEffect(() => {
        axios.get(pagesUrl)
            .then((res) => {
                const count = res.data.count;
                const totalPages = Math.ceil(count / parseInt(filter, 10));
                setPages(totalPages);
            });
    }, [filter]);

    // Delete an Item from Database
    const handleDeleteItem = (id) => {
        confirmAlert({
            title: 'Confirm to delete item',
            message: 'Are you sure you want to delete this item?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        axios.delete(`${baseUrl}/item/${id}`
                        )
                            .then((res) => {
                                const remainingItems = items.filter(
                                    (item) => item._id !== id
                                );
                                setItems(remainingItems);
                                toast.success('Item Deleted Successfully!', {
                                    position: "top-right"
                                });
                                // window.location.reload(false);
                            })
                            .catch((err) => {
                                toast.error(err);
                            })
                    },
                },
                {
                    label: 'No',
                    onClick: () => '',
                },
            ],
        });
    };

    const navigate = useNavigate();

    const handleChange = (event) => {
        const newPage = parseInt(event.target.textContent, 10);
        setCurrentPage(newPage);
        window.scrollTo(0, 0);
    };

    return (
        <Grid>
            <PageTitle title = {"Manage Inventory"} />
            <Header />
            <CssBaseline />
            <Box component = "main"
                 sx = {{
                     flexGrow: 1, bgcolor: '#F2F4F8', mt: '100px', maxWidth: 1400, margin: '0 auto'
                 }}>

                {/*Table Begin*/}
                <Grid className = 'table-bar'>
                    <Typography variant = "h1"
                                fontSize = {30}
                                fontWeight = {"bold"}
                                color = "#2c425d"
                                sx = {{ padding: "1rem 0" }}
                    >
                        Manage All Inventories
                    </Typography>

                    {/*Select / Show indicated Items Begin*/}
                    <Box sx = {{ minWidth: 70 }}>
                        <FormControl fullWidth>
                            <InputLabel id = "demo-simple-select-label">Show</InputLabel>
                            <Select
                                defaultValue = "10"
                                labelId = "demo-simple-select-label"
                                id = "demo-simple-select"
                                label = "show"
                                size = "small"
                                onChange = {(e) => setFilter(e.target.value)}
                            >
                                <MenuItem value = {10}>10</MenuItem>
                                <MenuItem value = {20}>20</MenuItem>
                                <MenuItem value = {50}>50</MenuItem>
                                <MenuItem value = {100}>100</MenuItem>
                                <MenuItem value = {200}>200</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                </Grid>

                <TableContainer>
                    {!items?.length ? (
                        <Loading height = {'50vh'}></Loading>
                    ) : (
                        <Table responsive = "sm" aria-label = "customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell>#</StyledTableCell>
                                    <StyledTableCell>Name</StyledTableCell>
                                    <StyledTableCell align = "right">Image</StyledTableCell>
                                    <StyledTableCell align = "right">Brand</StyledTableCell>
                                    <StyledTableCell align = "right">Category</StyledTableCell>
                                    <StyledTableCell align = "right">Buying price</StyledTableCell>
                                    <StyledTableCell align = "right">Selling Price</StyledTableCell>
                                    <StyledTableCell align = "center">Stock</StyledTableCell>
                                    <StyledTableCell align = "right">Supplier</StyledTableCell>
                                    <StyledTableCell align = "center">Action</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    items?.length > 0 && items?.map((item, index) => (
                                        <StyledTableRow hover
                                                        key = {item._id}
                                        >
                                            <ManageInventoryItem
                                                index = {index}
                                                item = {item}
                                                deleteItem = {handleDeleteItem}
                                                currentPage = {currentPage}
                                                filter = {filter}
                                            />
                                        </StyledTableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    )}
                </TableContainer>

                {/*Pagination Begin*/}
                <Grid display = "flex" justifyContent = "space-between" mt = '25px' mb = '5rem'>
                    {
                        <Pagination
                            onClick = {handleChange}
                            count = {pages}
                            color = "primary"
                        />

                    }

                    <Button sx = {{ padding: ".5rem 1.5rem" }} className = "manage-all-inventory-btn"
                            onClick = {() => {
                                navigate('/add-item')
                            }}
                    >
                        <AddCircleRoundedIcon sx = {{ marginRight: '.8rem' }} />
                        Add New Item
                    </Button>
                </Grid>

            </Box>
            <Footer />
        </Grid>
    );
};

export default ManageInventory;
