import React from 'react';
import { Chip, Tooltip } from "@mui/material";
import EditRoundedIcon from '@mui/icons-material/EditRounded';
import DeleteForeverRoundedIcon from '@mui/icons-material/DeleteForeverRounded';
import { useNavigate } from "react-router-dom";
import SyncRoundedIcon from '@mui/icons-material/SyncRounded';
import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';
import StyledTableCell from "../../../StyledComponents/StyledTableCell";


const ManageInventoryItem = ({ item, index, currentPage, filter, deleteItem }) => {

    const { _id, name, brand, category, buyingPrice, sellingPrice, stock, supplier, image } = item;
    // console.log({ item });

    const navigate = useNavigate();

    // Short Name
    const shortName = name?.slice(0, 50);

    // Comma added for price
    const buyingPriceWithComma = buyingPrice
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    const sellingPriceWithComma = sellingPrice
        ?.toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return (
        <>
            <StyledTableCell sx = {{ fontWeight: 'bold' }} align = "left">
                {(index + 1) + (currentPage - 1) * filter}.</StyledTableCell>
            <Tooltip title = {name} arrow>
                <StyledTableCell sx = {{ fontWeight: 'bold' }} scope = "row">{shortName}...</StyledTableCell>
            </Tooltip>
            <StyledTableCell width = "2rem" align = "right">
                <img width = "100%" src = {image} alt = "" />
            </StyledTableCell>
            <StyledTableCell align = "right">{brand}</StyledTableCell>
            <StyledTableCell align = "right">{category}</StyledTableCell>
            <StyledTableCell align = "right">${buyingPriceWithComma}</StyledTableCell>
            <StyledTableCell align = "right">${sellingPriceWithComma}</StyledTableCell>
            <StyledTableCell align = "center">
                {
                    stock ? stock :
                        <Chip
                            sx = {{ fontWeight: 'bold' }}
                            label = "Sold Out"
                            variant = "outlined"
                            color = 'error'
                        />
                }
            </StyledTableCell>
            <StyledTableCell align = "right">{supplier}</StyledTableCell>
            <StyledTableCell align = "right">
                <Tooltip title = "Deliver Product">
                    <LocalShippingOutlinedIcon color = "secondary"
                                               sx = {{ cursor: 'pointer' }}
                                               onClick = {() => navigate(`/inventory/product-delivery/${_id}`)}
                    />
                </Tooltip>{" "}
                <Tooltip title = "Update Stock">
                    <SyncRoundedIcon color = "success"
                                     sx = {{ cursor: 'pointer' }}
                                     onClick = {() => navigate(`/inventory/stock-update/${_id}`)}
                    />
                </Tooltip>{" "}
                <Tooltip title = "Edit Product Details">
                    <EditRoundedIcon
                        sx = {{ color: '#1976D2', cursor: 'pointer' }}
                        onClick = {() => navigate(`/updateProduct/${_id}`)}
                    />
                </Tooltip>{"  "}

                <Tooltip title = "Delete Product">
                    <DeleteForeverRoundedIcon
                        sx = {{ color: 'red', cursor: 'pointer' }}
                        onClick = {() => deleteItem(_id)}
                    />
                </Tooltip>
            </StyledTableCell>
        </>
    );
};

export default ManageInventoryItem;
