import React, { useEffect, useState } from 'react';
import "./AddItem.css";
import { useForm } from "react-hook-form";
import { useAuthState } from "react-firebase-hooks/auth";
import { Button, Container, Form } from "react-bootstrap";
import auth from "../../../firebase.init";
import { signOut } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import axiosPrivate from "../../../api/axiosPrivate";
import toast from "react-hot-toast";
import PageTitle from "../../Shared/PageTitle/PageTitle";
import Header from "../../Shared/Header/Header";


const AddItem = () => {
    const { register, handleSubmit, reset } = useForm();
    const [brands, setBrands] = useState([]);
    const [suppliers, setSuppliers] = useState([]);
    const [categories, setCategories] = useState([]);
    const [user] = useAuthState(auth);
    const navigate = useNavigate();
    const baseUrl = process.env.REACT_APP_BASE_URL;

    //Brand Name Request
    useEffect(() => {
        const getBrands = async () => {
            const url = `${baseUrl}/brand`;
            try {
                const { data } = await axiosPrivate.get(url);
                setBrands(data);
            } catch (error) {
                if (error.response.status === 401 || error.response.status === 403) {
                    await signOut(auth);
                    navigate('/login');
                }
            }
        };
        getBrands();
    }, [user]);

    //Category Request
    useEffect(() => {
        const getCategory = async () => {
            const url = `${baseUrl}/category`;
            try {
                const { data } = await axiosPrivate.get(url);
                setCategories(data);
            } catch (error) {
                if (error.response.status === 401 || error.response.status === 403) {
                    await signOut(auth);
                    navigate('/login');
                }
            }
        };
        getCategory();
    }, [user]);

    //Supplier Name Request
    useEffect(() => {
        const getSuppliers = async () => {
            const url = `${baseUrl}/supplier`;
            try {
                const { data } = await axiosPrivate.get(url);
                setSuppliers(data);
            } catch (error) {
                if (error.response.status === 401 || error.response.status === 403) {
                    await signOut(auth);
                    navigate('/login');
                }
            }
        };
        getSuppliers();
    }, [user]);

    //Data Validation
    const dataValidation = (itemData) => {
        if (itemData.brand === "Brand") {
            toast.error('Select Brand', {
                position: "top-center"
            });
            return true;
        } else if (itemData.category === "Category List") {
            toast.error('Select Category', {
                position: "top-center"
            });
            return true;
        } else if (itemData.supplier === "Suppliers' Name") {
            toast.error('Select Supplier Name', {
                position: "top-center"
            });
            return true;
        } else {
            return false;
        }
    };

    const onSubmit = data => {
        const temp = { ...data }
        temp.buyingPrice = parseInt(temp?.buyingPrice);
        temp.sellingPrice = parseInt(temp?.sellingPrice);
        temp.stock = parseInt(temp?.stock);

        const isVerified = dataValidation(temp);
        console.log({temp})

        if (!isVerified) {
            const url = `${baseUrl}/items`;
            fetch(url, {
                method: "POST",
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(temp)
            })
                .then(res => res.json())
                .then(result => {
                    if (result.insertedId) {
                        toast.success('Your Item has Been Added');
                        reset();
                    }
                    // console.log('Result: ', result);
                });
        }
    };


    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container w-50 mx-auto">
                <PageTitle title = {"Add Item"}></PageTitle>

                <Container>
                    <Form className = "add-item-border" onSubmit = {handleSubmit(onSubmit)}>
                        <h4 className = " title-color text-center mb-4"
                            style = {{ fontWeight: "bold", marginTop: "1rem" }}>Add New Item</h4>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text"
                                          placeholder = "Product Name" {...register("name", {
                                required: true
                            })} />

                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicBrand">
                            <Form.Select id = "brandList" {...register("brand", {
                                required: true
                            })}>
                                <option>Brand</option>
                                {
                                    brands?.map(brand => <option
                                        key = {brand._id}
                                    >{brand.name}
                                    </option>)
                                }
                            </Form.Select>

                            <Form.Select style = {{ marginLeft: "1rem" }} id = "categoryList" {...register("category", {
                                required: true
                            })}>
                                <option>Category List</option>
                                {
                                    categories?.map(category => <option
                                        key = {category._id}
                                    >{category.name}
                                    </option>)
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicPrice">
                            <Form.Control type = "number"
                                          placeholder = "Buying Price" {...register("buyingPrice", {
                                required: true,
                                maxLength: 20
                            })} />

                            <Form.Control style = {{ marginLeft: "1rem" }} type = "number"
                                          placeholder = "Selling Price" {...register("sellingPrice", {
                                required: true,
                                maxLength: 20
                            })} />

                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "formBasicStock">
                            <Form.Control type = "number"
                                          placeholder = "Stock" {...register("stock", {
                                required: true,
                                maxLength: 20
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "formBasicSupplier">
                            <Form.Select id = "suppliersList"
                                         {...register("supplier", {
                                             required: true
                                         })}>
                                <option>Suppliers' Name</option>
                                {
                                    suppliers?.map(supplier => <option
                                        key = {supplier._id}
                                    >{supplier.name}
                                    </option>)
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "formBasicEmail">
                            <Form.Control type = "email"
                                          readOnly
                                          placeholder = "Enter Email"
                                          value = {user?.email}
                                          {...register("email")} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "description">
                            <Form.Control placeholder = "Description" as = "textarea"
                                          rows = {3} {...register("description", {
                                required: true
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "imageUrl">
                            <Form.Control type = "text"
                                          placeholder = "Enter Image URL" {...register("image", {
                                required: true,
                            })} />
                        </Form.Group>

                        <Button className = "add-item-btn" style = {{ width: "60%" }}
                                variant = "primary mx-auto d-block mb-5" type = "submit">
                            Add Item
                        </Button>
                    </Form>
                </Container>
            </div>
        </>
    );
};

export default AddItem;
