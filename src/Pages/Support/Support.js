import React from 'react';
import "./Support.css";
import Header from "../Shared/Header/Header";
import PageTitle from "../Shared/PageTitle/PageTitle";
import { Button, Container, Form } from "react-bootstrap";
import toast from "react-hot-toast";
import { useForm } from "react-hook-form";
import { useAuthState } from "react-firebase-hooks/auth";
import auth from "../../firebase.init";

const Support = () => {
    const { register, handleSubmit, reset } = useForm();
    const [user] = useAuthState(auth);
    const baseUrl = process.env.REACT_APP_BASE_URL;

    //Data Validation
    const dataValidation = (messageData) => {
        if (messageData?.name === "") {
            toast.error('Name Required', {
                position: "top-center"
            });
            return true;
        } else if (messageData?.companyName === "") {
            toast.error('Company Name Required', {
                position: "top-center"
            });
            return true;
        } else if (messageData?.email === "") {
            toast.error('Email Required', {
                position: "top-center"
            });
            return true;
        } else if ((messageData?.contactNumber).toString === "") {
            toast.error('Contact Number Required', {
                position: "top-center"
            });
            return true;
        }else if (messageData?.message === "") {
            toast.error('Please write your message', {
                position: "top-center"
            });
            return true;
        }

        else {
            return false;
        }
    };

    const onSubmit = data => {
        // console.log(data);
        const isVerified = dataValidation(data);

        if (!isVerified && user?.displayName) {
            const url = `${baseUrl}/support`;
            fetch(url, {
                method: "POST",
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(result => {
                    if (result?.insertedId) {
                        toast.success('Message Has Been Sent');
                        reset();
                    }
                    // console.log('Result: ', result);
                });
        } else {
            toast.error('Please Login First');
        }
    };

    return (
        <>
            <div>
                <Header />
            </div>
            <div className = "container w-50 mx-auto">
                <PageTitle title = {"Support"}></PageTitle>

                <Container>
                    <Form className = "add-item-border" onSubmit = {handleSubmit(onSubmit)}>
                        <h4 className = " title-color text-center mb-4"
                            style = {{ fontWeight: "bold", marginTop: "1rem" }}>Contact Us</h4>

                        <Form.Group className = "mb-3 col-lg forms-control" controlId = "formBasicName">
                            <Form.Control type = "text"
                                          placeholder = "Name" {...register("name", {
                                required: true
                            })} />

                        </Form.Group>

                        <Form.Group className = "make-flex mb-3 col-lg forms-control" controlId = "formBasicPrice">
                            <Form.Control type = "text"
                                          placeholder = "Company" {...register("companyName", {
                                required: true,
                                maxLength: 20
                            })} />
                        </Form.Group>


                        <Form.Group className = "mb-3 forms-control" controlId = "formBasicEmail">
                            <Form.Control type = "email"
                                          placeholder = "Email"
                                          {...register("email", {
                                              required: true})} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "formBasicStock">
                            <Form.Control type = "number"
                                          placeholder = "Contact Number" {...register("contactNumber", {
                                required: true,
                                maxLength: 20
                            })} />
                        </Form.Group>

                        <Form.Group className = "mb-3 forms-control" controlId = "description">
                            <Form.Control placeholder = "Message" as = "textarea"
                                          rows = {4} {...register("message", {
                                required: true,
                                minLength:10
                            })} />
                        </Form.Group>

                        <Button className = "add-item-btn" style = {{ width: "60%" }}
                                variant = "primary mx-auto d-block mb-5" type = "submit">
                            Send Message
                        </Button>
                    </Form>
                </Container>
            </div>
        </>
    );
};

export default Support;
