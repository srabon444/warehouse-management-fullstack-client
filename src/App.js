import React, {Suspense, lazy} from 'react';
import { Route, Routes } from "react-router-dom";
import Home from "./Pages/Home/Home/Home";
import NotFound from "./Pages/Shared/NotFound/NotFound";
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
// import ManageInventory from "./Pages/ManageInventory/ManageInventory/ManageInventory";
import Login from "./Pages/Login/Login/Login";
import Registration from "./Pages/Login/Registration/Registration";
import AddItem from "./Pages/ManageInventory/AddItem/AddItem";
import { Toaster } from "react-hot-toast";
import ForgetPassword from "./Pages/Login/ForgetPassword/ForgetPassword";
import DeliverProduct from "./Pages/DeliverProduct/DeliverProduct";
// import ManageEmployees from "./Pages/ManageEmployees/ManageEmployees/ManageEmployees";
import AddEmployees from "./Pages/ManageEmployees/AddEmployees/AddEmployees";
import UpdateEmployeeDetails from "./Pages/UpdateEmployeeDetails/UpdateEmployeeDetails";
import ManageSuppliers from "./Pages/ManageSuppliers/ManageSuppliers/ManageSuppliers";
import AddSuppliers from "./Pages/ManageSuppliers/AddSuplliers/AddSuplliers";
import UpdateSupplier from "./Pages/UpdateSupplier/UpdateSupplier";
import UpdateProductDetails from "./Pages/UpdateProductDetails/UpdateProductDetails";
import ManageDelivery from "./Pages/ManageDelivery/ManageDelivery/ManageDelivery";
import DeliveryReport from "./Pages/DeliveryReport/DeliveryReport";
import Loading from "./Pages/Shared/Loading/Loading";
import Faqs from "./Pages/Faqs/Faqs";
import Support from "./Pages/Support/Support";
import ManageClients from "./Pages/ManageClients/ManageClients/ManageClients";
import AddClient from "./Pages/ManageClients/AddClient/AddClient";
import UpdateClientDetails from "./Pages/UpdateClientDetails/UpdateClientDetails";
import Invoice from "./Pages/Invoice/Invoice";
import ManageDashboard from "./Pages/ManageDashboard/ManageDashboard/ManageDashboard";


const LazyManageEmployees = lazy(() => import("./Pages/ManageEmployees/ManageEmployees/ManageEmployees"));
const LazyManageInventory = lazy(() => import("./Pages/ManageInventory/ManageInventory/ManageInventory"));

function App() {

    return (

        <div className = "App"
             style = {{ backgroundColor: '#F2F4F8' }}
        >
            <Routes>
                <Route path = "/" element = {<Home />} />

                <Route path = "/login" element = {<Login />} />
                <Route path = "/registration" element = {<Registration />} />
                <Route path = "/forgot-password" element = {<ForgetPassword />} />

                <Route path = "/manage-inventory" element =
                    {
                        <Suspense fallback={<Loading/>}>
                            <LazyManageInventory />
                        </Suspense>
                    } />
                <Route exact path = "/manage-employee" element = {
                    <Suspense fallback = {<Loading />}>
                        <LazyManageEmployees />
                    </Suspense>
                } />
                <Route path = "/suppliers" element = {<ManageSuppliers />} />
                <Route path = "/deliveries" element = {<ManageDelivery />} />

                <Route exact path = "/inventory/:actionType/:itemId" element = {<DeliverProduct />} />

                <Route path = "/add-employee" element = {<AddEmployees />} />
                <Route path = "/employee/:employeeId" element = {<UpdateEmployeeDetails />} />

                <Route path = "/add-item" element = {<AddItem />} />
                <Route path = "/updateProduct/:productId" element = {<UpdateProductDetails />} />

                <Route path = "/add-supplier" element = {<AddSuppliers />} />
                <Route path = "/supplier/:supplierId" element = {<UpdateSupplier />} />
                <Route path = "/delivery-details/:deliveryId" element = {<DeliveryReport />} />

                <Route path="/manage-clients" element={<ManageClients/>}/>
                <Route path="/add-client" element={<AddClient/>}/>
                <Route path="/client/:clientId" element={<UpdateClientDetails/>}/>

                <Route path="/invoice" element={<Invoice/>}/>
                <Route path="/dashboard" element={<ManageDashboard/>}/>

                <Route path="/faqs" element={<Faqs/>}/>
                <Route path="/support" element={<Support/>}/>
                <Route path = "/not-found" element = {<NotFound />} />
                <Route path = "*" element = {<NotFound />} />
            </Routes>
            <Toaster />
        </div>
    );
}

export default App;
