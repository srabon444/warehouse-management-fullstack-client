import { useEffect, useState } from "react";
import axios from "axios";

const useToken = user => {
    const [token, setToken] = useState('');
    const baseUrl = process.env.REACT_APP_BASE_URL;

    useEffect(() => {
        const getToken = async () => {
            // console.log("Before login from useToken", user);
            const email = user?.user?.email;
            // console.log({ email });
            if (email) {
                const { data } = await axios.post(`${baseUrl}/login`, {
                    email
                });
                // console.log({ data });
                setToken(data.accessToken);
                localStorage.setItem('accessToken', data.accessToken);
            }
        };
        getToken()
            .then();
    }, [user]);

    return [token];
};

export default useToken;