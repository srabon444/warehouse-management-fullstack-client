import { useEffect, useState } from "react";
import axios from "axios";

const useItems = (url) => {
    const [items, setItems] = useState([]);

    useEffect(() => {
        axios?.get(url)
            .then((res) => setItems(res?.data))
            .catch((err) => console.log(err));
    }, [url]);

    return [items, setItems];
};

export default useItems;