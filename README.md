# Shipido

An Inventory Management platform built with MERN stack. 

## ** Because of Vercel issue, please reload the live link 2-3 times if live website doesn't appear **


## [Live Application](https://jozzby-92a66.web.app/)


## [Server Side Repo](https://gitlab.com/srabon444/warehouse-management-fullstack-server/-/tree/master/)

![App Screenshot 1](1.png)
![App Screenshot 2](2.png)

## Features

• Authentication with JWT Web Token for login, signup, and verification
• Password update for users
• Dashboard with stock overview, categories, clients, suppliers, sales, purchases, and profit. Includes a table for low-stock products.
• Inventory management: add, edit, and delete products
• Product delivery option
• Stock management
• Table for deliveries
• Invoice generation and search
• Employee management: add, edit, and delete employees
• Client management: add, edit, and delete clients

## Tech Stack

**Fronted:** 
- React Js (create-react-app)
- Axios
- React Query
- Material UI
- Material Icons
- React Hook Form
- React Firebase Hooks

**Backend:** 
- Node Js
- Express Js
- MongoDB
- JWT Authentication 

**Hosting:**
- Render
- Firebase

  
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/warehouse-management-fullstack-client.git
```

Go to the project directory

```bash
  cd warehouse-management-fullstack-client
```

Install dependencies

```bash
  npm install
```

  
## Sample login credentials:

```bash
xyz@gmail.com (Admin)
xyz123
```

